package proc

import (
	"fmt"
	"io"
	"text/tabwriter"
)

// A wrapper-structure of tabwriter.Writer to dump process stat.
type StatDumper struct {
	w    *tabwriter.Writer
	once bool
}

func NewStatDumper(w io.Writer) *StatDumper {
	return &StatDumper{
		w:    tabwriter.NewWriter(w, 5, 0, 0, ' ', tabwriter.TabIndent),
		once: true,
	}
}

func (sd *StatDumper) Add(stat *Stat) {

	const (
		header = "\t S  \t UID \t PID \t PPID \t PRI \t NI \t SZ \t WCHAN \t TTY \t TIME \t CMD \t\n"
		format = "\t %v \t %v  \t %v  \t %v   \t %v  \t %v \t %v \t %v    \t %v  \t %v   \t %v  \t\n"
	)

	if sd.once {
		fmt.Fprintf(sd.w, header)
		sd.once = false
	}

	fmt.Fprintf(sd.w, format,
		stat.State,
		stat.Uid,
		stat.Pid,
		stat.Ppid,
		stat.Priority,
		stat.Nice,
		stat.Size,
		stat.Wchan,
		stat.TtyName,
		stat.CpuTime,
		stat.Comm,
	)
}

func (sd *StatDumper) Dump() {
	sd.w.Flush()
}

// A wrapper-structure of tabwriter.Writer to dump opened file stat.
type OpenedFileStatDumper struct {
	w    *tabwriter.Writer
	once bool
}

func NewOpenedFileStatDumper(w io.Writer) *OpenedFileStatDumper {
	return &OpenedFileStatDumper{
		w:    tabwriter.NewWriter(w, 5, 0, 0, ' ', tabwriter.TabIndent),
		once: true,
	}
}

func (ofsd *OpenedFileStatDumper) Add(stat *OpenedFileStat) {

	const (
		header = "\t COMMAND \t PID \t UID \t FD \t TYPE \t DEVICE \t SIZE \t NAME \t\n"
		format = "\t %v      \t %v  \t %v  \t %v \t %v   \t %v     \t %v   \t %v   \t\n"
	)

	if ofsd.once {
		fmt.Fprintf(ofsd.w, header)
		ofsd.once = false
	}

	fmt.Fprintf(ofsd.w, format,
		stat.Proc.Comm,
		stat.Proc.Pid,
		stat.Proc.Uid,
		stat.Fd,
		stat.Type,
		stat.Dev,
		stat.Size,
		stat.Name,
	)
}

func (ofsd *OpenedFileStatDumper) Dump() {
	ofsd.w.Flush()
}
