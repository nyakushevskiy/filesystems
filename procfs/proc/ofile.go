package proc

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// An abstraction of file opened by the process.
type OpenedFile struct {
	proc     Proc
	mem      bool
	name     string
	linkname string
	stat     *OpenedFileStat
}

type OpenedFileStat struct {

	// Is the process, which opened the file.
	Proc *Stat

	// "mem"                      - if it's a memory-mapped file,
	// "[fd-number][access-char]" - if it's not.
	// An access character is one of ("r", "w", "u" or "-")
	Fd string

	// Is the type of the node associated with the file.
	Type string

	// Is the device associated with the opened file.
	Dev Dev

	// Size of file data.
	Size int64

	// Name of the file.
	Name string
}

// List files from /proc/[pid]/fd directory
func (p Proc) ListOpenedFiles() (ofiles []OpenedFile, err error) {

	files, err := ioutil.ReadDir(filepath.Join(p.path, "fd"))
	if err != nil {
		return nil, err
	}

	ofileRe := regexp.MustCompile("^[0-9]+$")

	for _, f := range files {
		name, err := os.Readlink(filepath.Join(p.path, "fd", f.Name()))
		if err == nil && ofileRe.MatchString(f.Name()) {
			ofiles = append(ofiles, OpenedFile{
				proc:     p,
				name:     name,
				linkname: f.Name(),
			})
		}
	}

	return ofiles, nil
}

// List files from /proc/[pid]/maps file
func (p Proc) ListOpenedMemFiles() (ofiles []OpenedFile, err error) {

	pstat, err := p.Stat()
	if err != nil {
		return nil, err
	}

	maps, err := p.FileScanner("maps", bufio.ScanLines)
	if err != nil {
		return nil, err
	}

	maps.Scan()
	var scans = true

	// should fetch properties, that is needed from /proc/[pid]/stat
	for scans {

		var l struct {
			AddrFrom int64
			AddrTo   int64
			Perm     string
			Offset   int64
			DevMajor uint32
			DevMinor uint32
			Inode    int
			Name     string
			Prefix   string
		}

		var msize int64
		for strings.HasSuffix(maps.Text(), l.Name) {
			fmt.Sscanf(maps.Text(), "%x-%x %s %x %d:%d %d %s",
				&l.AddrFrom,
				&l.AddrTo,
				&l.Perm,
				&l.Offset,
				&l.DevMajor,
				&l.DevMinor,
				&l.Inode,
				&l.Name)
			msize += l.AddrTo - l.AddrFrom
			if scans = maps.Scan(); !scans {
				break
			}
		}

		var ftype string
		var fsize int64

		fstat, err := os.Stat(l.Name)
		if err == nil {
			fsize = fstat.Size()
			switch {
			case (fstat.Mode() & os.ModeDir) != 0:
				ftype = "DIR"

			case (fstat.Mode() & os.ModeNamedPipe) != 0:
				ftype = "FIFO"

			case (fstat.Mode() & os.ModeSocket) != 0:
				ftype = "SOCK"

			case (fstat.Mode() & os.ModeCharDevice) != 0:
				ftype = "CHR"

			case (fstat.Mode() & os.ModeDevice) != 0:
				ftype = "DEV"

			case (fstat.Mode() & os.ModeSymlink) != 0:
				ftype = "LNK"

			default:
				ftype = "REG"
			}
		} else {
			fsize = msize
			ftype = "?"
		}

		of := OpenedFile{
			proc: p,
			mem:  true,
			name: l.Name,
			stat: &OpenedFileStat{
				Proc: pstat,
				Fd:   "mem",
				Type: ftype,
				Size: fsize,
				Dev:  MakeDev(l.DevMajor, l.DevMinor),
				Name: l.Name,
			},
		}

		ofiles = append(ofiles, of)
	}

	return ofiles, nil
}

func FilterOpenedFiles(ofiles []OpenedFile, ok func(of OpenedFile) bool) (fofiles []OpenedFile) {
	for _, of := range ofiles {
		if ok(of) {
			fofiles = append(fofiles, of)
		}
	}
	return fofiles
}

func (of OpenedFile) Name() string {
	return of.name
}

func (of OpenedFile) Stat() (ofstat *OpenedFileStat, err error) {

	if of.stat != nil {
		return of.stat, nil
	}

	ofstat = &OpenedFileStat{Name: of.Name()}

	ofstat.Proc, err = of.proc.Stat()
	if err != nil {
		return nil, err
	}

	fstat, err := os.Stat(filepath.Join(of.proc.path, "fd", of.linkname))
	if err != nil {
		return nil, err
	}

	ofstat.Size = fstat.Size()
	switch {
	case (fstat.Mode() & os.ModeDir) != 0:
		ofstat.Type = "DIR"

	case (fstat.Mode() & os.ModeNamedPipe) != 0:
		ofstat.Type = "FIFO"

	case (fstat.Mode() & os.ModeSocket) != 0:
		ofstat.Type = "SOCK"

	case (fstat.Mode() & os.ModeCharDevice) != 0:
		ofstat.Type = "CHR"

	case (fstat.Mode() & os.ModeDevice) != 0:
		ofstat.Type = "DEV"

	case (fstat.Mode() & os.ModeSymlink) != 0:
		ofstat.Type = "LNK"

	default:
		ofstat.Type = "REG"
	}

	fdinfo, err := of.proc.FileScanner(filepath.Join("fdinfo", of.linkname), bufio.ScanLines)
	if err != nil {
		return nil, err
	}

	var flags int
	var mntid int

	for fdinfo.Scan() {
		// This is an octal number that displays the file access mode and file status flags
		fmt.Sscanf(fdinfo.Text(), "flags:  %o", &flags)

		// This field is the ID of the mount point containing this file.
		fmt.Sscanf(fdinfo.Text(), "mnt_id: %d", &mntid)
	}

	var access string
	switch {
	case (flags & os.O_RDONLY) != 0:
		access = "r"

	case (flags & os.O_WRONLY) != 0:
		access = "w"

	case (flags & os.O_RDWR) != 0:
		access = "u"

	default:
		access = "-"
	}
	ofstat.Fd = of.linkname + access

	mountinfo, err := of.proc.FileScanner("mountinfo", bufio.ScanLines)
	if err != nil {
		return nil, err
	}

	for mountinfo.Scan() {

		var l struct {
			// A unique ID for the mount.
			MountId int

			// The ID of the parent mount.
			PmountId int

			// The value of st_dev for files on this filesystem.
			DevMajor, DevMinor uint32
		}

		fmt.Sscanf(mountinfo.Text(), "%d %d %d:%d",
			&l.MountId,
			&l.PmountId,
			&l.DevMajor,
			&l.DevMinor)

		if l.MountId == mntid {
			ofstat.Dev = MakeDev(l.DevMajor, l.DevMinor)
			break
		}
	}

	of.stat = ofstat

	return ofstat, nil
}
