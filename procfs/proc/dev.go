package proc

import (
	"bufio"
	"fmt"
)

// An abstraction for device.
type Dev int64

const (
	majorDevMask1  = 0x00000000000fff00
	majorDevShift1 = 8

	majorDevMask2  = 0xfffff00000000000
	majorDevShift2 = 32

	minorDevMask1  = 0x00000000000000ff
	minorDevShift1 = 0

	minorDevMask2  = 0x00000ffffff00000
	minorDevShift2 = 12
)

// Get the device by major and minor number.
func MakeDev(major, minor uint32) Dev {
	d := Dev((uint64(major) << majorDevShift1) & majorDevMask1)
	d |= Dev((uint64(major) << majorDevShift2) & majorDevMask2)
	d |= Dev((uint64(minor) << minorDevShift1) & minorDevMask1)
	d |= Dev((uint64(minor) << minorDevShift2) & minorDevMask2)
	return d
}

// Get the major device number.
func (d Dev) Major() uint32 {
	major := uint32((uint64(d) & majorDevMask1) >> majorDevShift1)
	major |= uint32((uint64(d) & majorDevMask2) >> majorDevShift2)
	return major
}

// Get the minor device number.
func (d Dev) Minor() uint32 {
	minor := uint32((uint64(d) & minorDevMask1) >> minorDevShift1)
	minor |= uint32((uint64(d) & minorDevMask2) >> minorDevShift2)
	return minor
}

// Get string representation of device.
func (d Dev) String() string {
	return fmt.Sprintf("%d,%d", d.Major(), d.Minor())
}

// Get the possible names of the device.
func (d Dev) PossibleNames() (names []string) {

	if d == -1 {
		return nil
	}

	sc, err := FileScanner("/proc/devices", bufio.ScanLines)
	if err != nil {
		return nil
	}

	m, name := 0, ""
	for sc.Scan() {
		fmt.Sscanf(sc.Text(), "%d %s", &m, &name)
		if uint32(m) == d.Major() {
			names = append(names, fmt.Sprint(name, d.Minor()))
		}
	}

	return names
}
