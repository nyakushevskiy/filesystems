package proc

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// An abstraction for proccess.
type Proc struct {
	path string
	stat *Stat
}

// Flags in ProcStat
const (
	NotExecuted = 0x1
	RunWithSudo = 0x4
)

// This info very similar to output by the "ps -l" format, but missing "F", "C" and "ADDR" columns.
// Collected stat of proccess from "/proc/[pid]/stat" and "/proc/[pid]/status".
type Stat struct {

	// Multi-character process state.
	State string

	// Efective user ID.
	Uid int

	// A number representing the process ID.
	Pid int

	// Parent process ID.
	Ppid int

	// Priority of the process.
	Priority int64

	// Nice value.
	Nice int64

	// Size in KB of the core image of the process.
	Size int64

	// This  is the "channel" in which the process is waiting.
	// It is the address of a location in the kernel where the process is sleeping.
	Wchan int64

	// Controlling tty name.
	TtyName string

	// Cumulative CPU time.
	CpuTime string

	// Command name (only the executable name).
	Comm string
}

// List proccesses from /proc directory.
func List() (procs []Proc, err error) {

	files, err := ioutil.ReadDir("/proc")
	if err != nil {
		return nil, err
	}

	procRe := regexp.MustCompile("^[0-9]+$")

	for _, f := range files {
		if ((f.Mode() & os.ModeDir) != 0) && procRe.MatchString(f.Name()) {
			path := filepath.Join("/proc", f.Name())
			procs = append(procs, Proc{path: path})
		}
	}

	return procs, err
}

func Filter(procs []Proc, ok func(p Proc) bool) (fprocs []Proc) {
	for _, p := range procs {
		if ok(p) {
			fprocs = append(fprocs, p)
		}
	}
	return fprocs
}

// Get the process PID.
func (p Proc) Pid() int {
	var pid int
	fmt.Sscanf(p.path, filepath.Join("/proc", "%d"), &pid)
	return pid
}

// Get the file scanner from the relative file name
func (p Proc) FileScanner(name string, split bufio.SplitFunc) (*bufio.Scanner, error) {
	return FileScanner(filepath.Join(p.path, name), split)
}

// Get the stat of the proccess.
func (p Proc) Stat() (*Stat, error) {

	if p.stat != nil {
		return p.stat, nil
	}

	pstat := &Stat{Pid: p.Pid()}

	// contains list of fields separated by spaces
	stat, err := p.FileScanner("stat", bufio.ScanWords)
	if err != nil {
		return nil, err
	}

	var time int64
	var ttynr int

	// should fetch properties, that is needed from /proc/[pid]/stat
	for prop := 1; stat.Scan(); prop++ {
		switch prop {
		case 2:
			// (2) comm  %s
			// The filename of the executable, in parentheses.
			pstat.Comm = strings.Trim(stat.Text(), "()")

		case 3:
			// (3) state  %c
			// One of the following characters, indicating process state.
			pstat.State = stat.Text()

		case 4:
			// (4) ppid  %d
			// The PID of the parent of this process.
			fmt.Sscanf(stat.Text(), "%d", &pstat.Ppid)

		case 7:
			// (7) tty_nr  %d
			// The controlling terminal of the process.
			fmt.Sscanf(stat.Text(), "%d", &ttynr)

		case 14:
			// (14) utime  %lu
			// Amount of time that this process has been scheduled in user mode, measured in clock ticks.
			var utime int64
			fmt.Sscanf(stat.Text(), "%d", &utime)
			time += utime

		case 15:
			// (15) stime  %lu
			// Amount of time that this process has been scheduled in kernel mode, measured in clock ticks
			var stime int64
			fmt.Sscanf(stat.Text(), "%d", &stime)
			time += stime

		case 18:
			// (18) priority  %ld
			// For processes running a real-time scheduling policy, this is the negated scheduling priority, minus one;
			// that is, a number in the range -2 to -100, corresponding to real-time priorities 1 to 99.
			fmt.Sscanf(stat.Text(), "%d", &pstat.Priority)

		case 19:
			// (19) nice  %ld
			// The nice value, a value in the range 19 (low priority) to -20 (high priority).
			fmt.Sscanf(stat.Text(), "%d", &pstat.Nice)

		case 35:
			// (35) wchan  %lu  [PT]
			// This is the "channel" in which the process is waiting.
			// It is the address of a location in the kernel where the process is sleeping.
			fmt.Sscanf(stat.Text(), "%d", &pstat.Wchan)
		}
	}

	names := FilterNamesBy("tty", Dev(ttynr).PossibleNames())
	if len(names) > 0 {
		pstat.TtyName = names[0]
	} else {
		pstat.TtyName = "?"
	}
	pstat.CpuTime = fmt.Sprintf("%v clocks", time)

	status, err := p.FileScanner("status", bufio.ScanLines)
	if err != nil {
		return nil, err
	}

	// should fetch properties, that is needed from /proc/[pid]/stat
	for status.Scan() {

		// Real UID.
		fmt.Sscanf(status.Text(), "Uid:    %d", &pstat.Uid)

		// Virtual memory size in KB.
		fmt.Sscanf(status.Text(), "VmSize: %d", &pstat.Size)
	}

	p.stat = pstat

	return pstat, nil
}
