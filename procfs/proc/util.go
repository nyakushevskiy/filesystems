package proc

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"regexp"
)

// Filter names from the list of possible names by regexp.
func FilterNamesBy(pattern string, names []string) []string {

	re, err := regexp.Compile(pattern)
	if err != nil {
		return names
	}

	var fnames []string
	for _, name := range names {
		if re.MatchString(name) {
			fnames = append(fnames, name)
		}
	}

	return fnames
}

// A wrapper-constructor of bufio.Scanner to parse files.
func FileScanner(name string, split bufio.SplitFunc) (*bufio.Scanner, error) {

	data, err := ioutil.ReadFile(name)
	if err != nil {
		return nil, err
	}

	const N = 256

	sc := bufio.NewScanner(bytes.NewReader(data))

	sc.Buffer(make([]byte, N), N)
	sc.Split(split)

	return sc, nil
}
