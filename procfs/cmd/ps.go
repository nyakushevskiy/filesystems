package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/filesystems/procfs/proc"
)

var psCmd = &cobra.Command{
	Use:   "ps",
	Short: `Simple implemntation of "ps" linux command`,
	Run:   ps,
}

func init() {
	rootCmd.AddCommand(psCmd)
}

func ps(cmd *cobra.Command, args []string) {

	d := proc.NewStatDumper(cmd.OutOrStdout())
	defer d.Dump()

	for _, p := range procs {
		pstat, warn := p.Stat()
		if warn != nil {
			fmt.Fprintf(cmd.OutOrStderr(),
				"Warning: while retreiving the stat of <%d> proccess: %v\n",
				p.Pid(), warn)
			continue
		}
		d.Add(pstat)
	}
}
