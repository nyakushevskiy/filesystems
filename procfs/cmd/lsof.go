package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/filesystems/procfs/proc"
)

var lsofCmd = &cobra.Command{
	Use:   "lsof",
	Short: `Simple implementation of "lsof" linux command`,
	Run:   lsof,
}

var nflag *bool

func init() {
	rootCmd.AddCommand(lsofCmd)
	nflag = lsofCmd.PersistentFlags().BoolP("no-maps", "n", false, "do not list memory mapped files")
}

func lsof(cmd *cobra.Command, args []string) {

	d := proc.NewOpenedFileStatDumper(cmd.OutOrStdout())
	defer d.Dump()

	for _, p := range procs {
		ofiles, warn := p.ListOpenedFiles()
		if warn != nil {
			fmt.Fprintf(cmd.OutOrStderr(),
				"Warning: while retreiving the opened files of <%d> processes: %v\n",
				p.Pid(), warn)
		}
		if n := *nflag; !n {
			omfiles, warn := p.ListOpenedMemFiles()
			if warn != nil {
				fmt.Fprintf(cmd.OutOrStderr(),
					"Warning: while retreiving the opened files of <%d> processes: %v\n",
					p.Pid(), warn)
			} else {
				ofiles = append(ofiles, omfiles...)
			}
		}
		for _, of := range ofiles {
			ofstat, warn := of.Stat()
			if warn != nil {
				fmt.Fprintf(cmd.OutOrStderr(),
					"Warning: while retreiving the stat of the opened file %q of <%d> process: %v\n",
					of.Name(), p.Pid(), warn)
				continue
			}
			d.Add(ofstat)
		}
	}
}
