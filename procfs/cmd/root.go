package cmd

import (
	"fmt"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/filesystems/procfs/proc"
)

var rootCmd = &cobra.Command{
	Use:   "procfs",
	Short: `ProcFs - is a simple implementations of linux commands, which dumps info from /proc file system.`,

	PersistentPreRunE: root,
}

func Execute() {
	rootCmd.Execute()
}

var procs []proc.Proc

var cflag *string
var uflag *int
var pflag *int

func init() {
	cflag = rootCmd.PersistentFlags().StringP("cmd", "c", "-", "list processes associated only with this cmd")
	uflag = rootCmd.PersistentFlags().IntP("uid", "u", -1, "list processes associated only with this uid")
	pflag = rootCmd.PersistentFlags().IntP("pid", "p", -1, "list processes associated only with this pid")
}

func root(cmd *cobra.Command, args []string) (err error) {

	procs, err = proc.List()
	if err != nil {
		return fmt.Errorf(`Error: while listing "/proc" directory:`, err)
	}

	if c := *cflag; c != "-" {
		procs = proc.Filter(procs, func(pr proc.Proc) bool {
			prstat, _ := pr.Stat()
			return (prstat != nil) && prstat.Comm == c
		})
	}

	if u := *uflag; u != -1 {
		procs = proc.Filter(procs, func(pr proc.Proc) bool {
			prstat, _ := pr.Stat()
			return (prstat != nil) && prstat.Uid == u
		})
	}

	if p := *pflag; p != -1 {
		procs = proc.Filter(procs, func(pr proc.Proc) bool {
			prstat, _ := pr.Stat()
			return (prstat != nil) && prstat.Pid == p
		})
	}

	return nil
}
