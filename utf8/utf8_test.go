package utf8_test

import (
	"testing"

	"bitbucket.org/nyakushevskiy/filesystems/utf8"
)

func TestEncodeDecode(t *testing.T) {

	const input = "Hello, 世界"

	bytes, err := utf8.Decode([]rune(input))
	if err != nil {
		t.Fatalf("Unexpected error when decoding %q: %v!", input, err)
	}

	output, err := utf8.Encode(bytes)
	if err != nil {
		t.Fatalf("Unexpected error when encoding %q: %v!", input, err)
	}

	if string(output) != input {
		t.Fatalf("Unexpected result of encoding! got: %q, expected: %q", string(output), input)
	}
}
