package utf8

import "fmt"

// rune is an alias for uint32
// byte is an alias for uint8

// number of significant bits in utf-8 encoding bytes
const (
	FIRST_BITS1 = 7 - iota // in first byte in unicode character encoding with a length of 1
	EXTRA_BITS             // in extra byte (1-3) in unicode character byte code
	FIRST_BITS2            // in first byte in unicode character encoding with a length of 2
	FIRST_BITS3            // in first byte in unicode character encoding with a length of 3
	FIRST_BITS4            // in first byte in unicode character encoding with a length of 4
)

// significant bits mask in utf-8 encoding bytes
const (
	FIRST_MASK1 = (1 << FIRST_BITS1) - 1 // 0b0xxxxxxxx
	EXTRA_MASK  = (1 << EXTRA_BITS) - 1  // 0b10xxxxxxx
	FIRST_MASK2 = (1 << FIRST_BITS2) - 1 // 0b110xxxxxx
	FIRST_MASK3 = (1 << FIRST_BITS3) - 1 // 0b1110xxxxx
	FIRST_MASK4 = (1 << FIRST_BITS4) - 1 // 0b11110xxxx
)

// ranges of unicode characters
const (
	RANGE1 = (1 << (FIRST_BITS1 + 0*EXTRA_BITS)) - 1 // 0x00000000-0x0000007f
	RANGE2 = (1 << (FIRST_BITS2 + 1*EXTRA_BITS)) - 1 // 0x00000080-0x000007ff
	RANGE3 = (1 << (FIRST_BITS3 + 2*EXTRA_BITS)) - 1 // 0x00000800-0x0000ffff
	RANGE4 = (1 << (FIRST_BITS4 + 3*EXTRA_BITS)) - 1 // 0x00010000-0x001fffff
)

// markers of utf-8 encoding bytes, where the size is encoded
const (
	FIRST_MARKER1 = (1<<(7-FIRST_BITS1) - 1) << (FIRST_BITS1 + 1) // 0b000000000
	EXTRA_MARKER  = (1<<(7-EXTRA_BITS) - 1) << (EXTRA_BITS + 1)   // 0b100000000
	FIRST_MARKER2 = (1<<(7-FIRST_BITS2) - 1) << (FIRST_BITS2 + 1) // 0b110000000
	FIRST_MARKER3 = (1<<(7-FIRST_BITS3) - 1) << (FIRST_BITS3 + 1) // 0b111000000
	FIRST_MARKER4 = (1<<(7-FIRST_BITS4) - 1) << (FIRST_BITS4 + 1) // 0b111100000
)

// decodes one unicode character to utf-8 bytes sequence
func decodeRune(r rune) (bytes []byte, err error) {

	switch {
	case r <= RANGE1:
		bytes = []byte{
			FIRST_MARKER1, // 0b0xxxxxxx
		}
	case r <= RANGE2:
		bytes = []byte{
			FIRST_MARKER2, // 0b110xxxxx
			EXTRA_MARKER,  // 0b10xxxxxx
		}
	case r <= RANGE3:
		bytes = []byte{
			FIRST_MARKER3, // 0b1110xxxx
			EXTRA_MARKER,  // 0b10xxxxxx
			EXTRA_MARKER,  // 0b10xxxxxx
		}
	case r <= RANGE4:
		bytes = []byte{
			FIRST_MARKER4, // 0b11110xxx
			EXTRA_MARKER,  // 0b10xxxxxx
			EXTRA_MARKER,  // 0b10xxxxxx
			EXTRA_MARKER,  // 0b10xxxxxx
		}
	default:
		err = fmt.Errorf("invalid utf-8 character U+%x", r)
		return
	}

	for i := len(bytes) - 1; i > 0; i-- {
		bytes[i] |= byte(r) & EXTRA_MASK
		r >>= EXTRA_BITS
	}
	bytes[0] |= byte(r)

	return bytes, err
}

// decodes one unicode character to utf-8 bytes sequence
func Decode(runes []rune) (bytes []byte, err error) {
	var b []byte
	for _, r := range runes {
		b, err = decodeRune(r)
		if err != nil {
			break
		}
		bytes = append(bytes, b...)
	}
	return bytes, err
}

// encodeRune encodes one utf-8 character from the start of bytes
func encodeRune(bytes []byte) (r rune, n int, err error) {

	switch {
	case (bytes[0] &^ FIRST_MASK1) == FIRST_MARKER1: // 0b0xxxxxxx
		r = rune(bytes[0] & FIRST_MASK1)
		n = 1

	case (bytes[0] &^ FIRST_MASK2) == FIRST_MARKER2: // 0b110xxxxx
		r = rune(bytes[0] & FIRST_MASK2)
		n = 2

	case (bytes[0] &^ FIRST_MASK3) == FIRST_MARKER3: // 0b1110xxxx
		r = rune(bytes[0] & FIRST_MASK3)
		n = 3

	case (bytes[0] &^ FIRST_MASK4) == FIRST_MARKER4: // 0b11110xxx
		r = rune(bytes[0] & FIRST_MASK4)
		n = 4

	default:
		err = fmt.Errorf("invalid first byte 0b%b", bytes[0])
		return
	}

	if n > len(bytes) {
		err = fmt.Errorf("unexpected end of bytes sequence")
	}

	for i := 1; i < n; i++ {
		if (bytes[i] &^ EXTRA_MASK) != EXTRA_MARKER { // 0x10xxxxxx
			err = fmt.Errorf("invalid extra byte 0b%b", bytes[i])
			return
		}
		r <<= EXTRA_BITS
		r |= rune(bytes[i] & EXTRA_MASK)
	}

	return r, n, err
}

// Encode encodes utf-8 bytes to unicode characters
func Encode(bytes []byte) (runes []rune, err error) {
	var r rune
	for i, n := 0, 0; i < len(bytes); i += n {
		r, n, err = encodeRune(bytes[i:])
		if err != nil {
			break
		}
		runes = append(runes, r)
	}
	return runes, err
}
