package cmd

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
)

var inodeCmd = &cobra.Command{
	Use:   "inode",
	Short: "dump inode info!",

	RunE: DumpInode,
}

var inodeNum int64

func init() {
	rootCmd.AddCommand(inodeCmd)
	inodeCmd.Flags().Int64VarP(&inodeNum, "num", "n", -1, "number of an inode to do a dump of (starts from 1)")
}

func DumpInode(cmd *cobra.Command, _ []string) error {

	if inodeNum == -1 {
		return fmt.Errorf(`--num (or -n) flag was not specified, but is required`)
	}

	inode, err := fs.Inode(inodeNum)
	if err != nil {
		return fmt.Errorf("while searching for an inode: %v", err)
	}

	entry := inode.RawEntry

	var T string
	switch {
	case inode.Mode.IsRegular():
		T = "regular"
	case inode.Mode.IsDir():
		T = "directory"
	default:
		T = "special"
	}

	const format = `Inode: %d
Type: %s Mode: 0%3o, Flags: 0x%x
Generation: %d   Version:  0x%.8x:%.8x
User:       %d   Group: %d   Size: %d
File ACL:   %d
Links: %d   Blockcount: %d
Fragment Address: %d
  ctime: 0x%.8x:%.8x -- %s
  atime: 0x%.8x:%.8x -- %s
  mtime: 0x%.8x:%.8x -- %s
 crtime: 0x%.8x:%.8x -- %s
Size of extra inode fields: %d
Inode checksum: 0x%x

`
	cmd.Printf(format, inodeNum,
		T, inode.Mode.Perm(), uint32(entry.Flags),
		entry.Generation, entry.VersionLow, entry.VersionHigh,
		uint64(entry.OwnerUidLow)|uint64(entry.OwnerUidHigh)<<16,
		uint64(entry.OwnerGidLow)|uint64(entry.OwnerGidHigh)<<16, inode.DataSize,
		uint64(entry.XattrLocationLow)|uint64(entry.XattrLocationHigh)<<32,
		entry.HardLinks, inode.DataBlocks,
		entry.FragmentAddress,
		uint32(entry.ChangeTime), uint32(entry.ChangeTimeExtraPrecision),
		entry.ChangeTime.Time().Format(time.UnixDate),
		uint32(entry.AccessTime), uint32(entry.AccessTimeExtraPrecision),
		entry.AccessTime.Time().Format(time.UnixDate),
		uint32(entry.ModTime), uint32(entry.ModTimeExtraPrecision),
		entry.ModTime.Time().Format(time.UnixDate),
		uint32(entry.CreationTime), uint32(entry.CreationTimeExtraPrecision),
		entry.CreationTime.Time().Format(time.UnixDate),
		entry.ExtraSize,
		uint32(entry.ChecksumLow)|uint32(entry.ChecksumHigh)<<16,
	)

	return nil
}
