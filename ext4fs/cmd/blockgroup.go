package cmd

import (
	"fmt"
	"unsafe"

	"github.com/spf13/cobra"
)

var blockGroupCmd = &cobra.Command{
	Use:   "blockgroup",
	Short: "dump block group info!",

	RunE: DumpBlockGroup,
}

var groupNum int64

func init() {
	rootCmd.AddCommand(blockGroupCmd)
	blockGroupCmd.Flags().Int64VarP(&groupNum, "num", "n", -1, "number of a block group to do a dump of (starts from 0)")
}

func DumpBlockGroup(cmd *cobra.Command, _ []string) error {

	if groupNum == -1 {
		return fmt.Errorf(`--num (or -n) flag was not specified, but is required`)
	}

	group, err := fs.BlockGroup(groupNum)
	if err != nil {
		cmd.Println("while searching for a block group:", err)
		return nil
	}

	freeBlockRanges, err := group.FreeBlocksRanges()
	for n := range freeBlockRanges {
		freeBlockRanges[n].Off += fs.BlocksPerGroup * group.Num
	}
	if err != nil {
		cmd.Println("while reading free block ranges:", err)
		return nil
	}

	freeInodeRanges, err := group.FreeInodesRanges()
	for n := range freeInodeRanges {
		freeInodeRanges[n].Off += fs.InodesPerGroup*group.Num + 1
	}
	if err != nil {
		cmd.Println("while reading free inode ranges:", err)
		return nil
	}

	ds := fs.GroupDescSize
	desc := group.RawDesc

	freeBlocks := int32(desc.FreeBlocksLow)
	if unsafe.Offsetof(desc.FreeBlocksHigh)+2 <= uintptr(ds) {
		freeBlocks |= int32(desc.FreeBlocksHigh) << 16
	}

	freeInodes := int32(desc.FreeInodesLow)
	if unsafe.Offsetof(desc.FreeInodesHigh)+2 <= uintptr(ds) {
		freeInodes |= int32(desc.FreeInodesHigh) << 16
	}

	usedDirectories := int32(desc.UsedDirectoriesLow)
	if unsafe.Offsetof(desc.UsedDirectoriesHigh)+2 <= uintptr(ds) {
		usedDirectories |= int32(desc.UsedDirectoriesHigh) << 16
	}

	unusedInodes := int32(desc.UnusedInodesLow)
	if unsafe.Offsetof(desc.UnusedInodesHigh)+2 <= uintptr(ds) {
		unusedInodes |= int32(desc.UnusedInodesHigh) << 16
	}

	const format = `Group %v: (Blocks %v-%v) checksum 0x%x %v
	Block bitmap at %v, checksum 0x%x
	Inode bitmap at %v, checksum 0x%x
	Inode table at %v
	%d free blocks, %d free inodes, %d used directories, %d unused inodes
	Free blocks: %v
	Free inodes: %v

`
	n := group.Num

	cmd.Printf(format, n, fs.BlocksPerGroup*(n+1), fs.BlocksPerGroup*(n+1)-1, desc.Checksum, desc.Flags.Names(),
		group.BlockBitmapLocation, group.BlockBitmapChecksum,
		group.InodeBitmapLocation, group.InodeBitmapChecksum,
		group.InodeTableLocation,
		freeBlocks, freeInodes, usedDirectories, unusedInodes,
		freeBlockRanges,
		freeInodeRanges,
	)

	return nil
}
