package cmd

import (
	"bytes"
	"fmt"
	"io"
	"path/filepath"
	"text/tabwriter"
	"time"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/filesystems/ext4fs/ext4"
)

var fileCmd = &cobra.Command{
	Use:   "file",
	Short: "dump a file or a directory!",

	RunE: DumpFile,
}

var fileName string
var extents bool

func init() {
	rootCmd.AddCommand(fileCmd)
	fileCmd.Flags().StringVarP(&fileName, "name", "n", "", "path to a regular file or a directory to do a dump of")
	fileCmd.Flags().BoolVarP(&extents, "extents", "e", false, "to dump file blocks also")
}

func FdumpFileExtents(w io.Writer, fs *ext4.Ext4, name string) error {

	file, err := fs.OpenFile(name)
	if err != nil {
		return err
	}

	const format = `Dump of %q file extents!
%v

`

	buf := new(bytes.Buffer)
	tw := tabwriter.NewWriter(buf, 5, 0, 0, ' ', tabwriter.TabIndent)

	n := int64(0)
	offset, length := int64(0), int64(0)

	fmt.Fprintf(tw, " \t Offset: \t Length: \t\n")

	file.ForEachBlock(func(_ []byte, location int64) bool {
		if offset+length == location || (location <= 0 && offset <= 0) {
			length++
		} else {
			if n > 0 {
				fmt.Fprintf(tw, "<%v>\t %v \t %v \t\n", n-length, offset, length)
			}
			offset, length = location, 1
		}
		n++
		return true
	})

	if length > 0 {
		fmt.Fprintf(tw, "<%v>\t %v \t %v \t\n", n-length, offset, length)
	}
	tw.Flush()

	content := buf.String()
	if len(content) == 0 {
		content = "<none>"
	}

	fmt.Fprintf(w, format, file.Name(), content)

	return nil
}

func FdumpFileContent(w io.Writer, fs *ext4.Ext4, name string) error {

	file, err := fs.OpenFile(name)
	if err != nil {
		return err
	}

	info, err := file.Stat()
	if err != nil {
		return err
	}

	const format = `Dump of %q file stat!
	Size:             %v
	Modificated at:   %v
	Mode:             %v

`
	fmt.Fprintf(w, format,
		info.Name,
		info.Size,
		info.ModTime.Format(time.UnixDate),
		info.Mode,
	)

	if file.IsDir() {
		return fdumpDir(w, file)
	} else {
		return fdumpRegular(w, file)
	}
}

// fdumpDir dumps directory dir to w.
func fdumpDir(w io.Writer, dir *ext4.File) error {

	files, err := dir.ListFiles()
	if err != nil {
		return err
	}

	const format = `Dump of %q directory content!
%v

`

	buf := new(bytes.Buffer)
	tw := tabwriter.NewWriter(buf, 5, 0, 0, ' ', tabwriter.TabIndent)

	if len(files) > 0 {
		fmt.Fprintf(tw, " Mode: \t Size: \t Modtime: \t Name: \t\n")
	}

	var file *ext4.File

	for _, file = range files {
		info, err := file.Stat()
		if err != nil {
			continue
		}
		fmt.Fprintf(tw, " %v    \t %v    \t %v       \t %v    \t\n",
			info.Mode,
			info.Size,
			info.ModTime.Format(time.UnixDate),
			filepath.Base(info.Name),
		)
	}

	tw.Flush()

	content := buf.String()
	if len(content) == 0 {
		content = "<none>"
	}

	fmt.Fprintf(w, format, dir.Name(), content)

	return nil
}

// fdumpRegular dumps regular file to w.
func fdumpRegular(w io.Writer, file *ext4.File) error {

	buf, err := file.ReadAll()
	if err != nil {
		return err
	}

	const format = `Dump of %q file content!
%v

`
	content := string(buf)
	if len(buf) == 0 {
		content = "<none>"
	}

	fmt.Fprintf(w, format, file.Name(), content)

	return nil
}

func DumpFile(cmd *cobra.Command, _ []string) error {

	if fileName == "" {
		return fmt.Errorf(`--name (or -n) flag was not specified, but is required`)
	}

	var err error
	if extents {
		err = FdumpFileExtents(cmd.OutOrStdout(), fs, fileName)
	} else {
		err = FdumpFileContent(cmd.OutOrStdout(), fs, fileName)
	}
	if err != nil {
		return fmt.Errorf("while dumping file %q: %v", fileName, err)
	}

	return nil
}
