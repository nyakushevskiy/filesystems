package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"bitbucket.org/nyakushevskiy/filesystems/ext4fs/ext4"
)

var rootCmd = &cobra.Command{
	Use:   "ext4fs",
	Short: "Ext4Fs - is a CLI-tool for dumping ext4 on disk data structures",

	PersistentPreRunE: OpenFs,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var fs *ext4.Ext4

var d string

func init() {
	rootCmd.PersistentFlags().StringVarP(&d, "dev", "d", "", "name of the block device of the partition formatted with ext4")
}

func OpenFs(cmd *cobra.Command, args []string) error {

	if d == "" {
		return fmt.Errorf(`--dev (or -d) flag was not specified, but is required`)
	}

	blkdev, err := os.Open(d)
	if err != nil {
		return fmt.Errorf("when openning the block device %q: %v\n", d, err)
	}

	fs, err = ext4.Open(blkdev)
	if err != nil {
		return fmt.Errorf("when openning the fs on the block device %q: %v\n", d, err)
	}

	return nil
}
