package cmd

import (
	"github.com/spf13/cobra"
)

var superBlockCmd = &cobra.Command{
	Use:   "superblock",
	Short: "dump super block info!",

	RunE: DumpSuperBlock,
}

func init() {
	rootCmd.AddCommand(superBlockCmd)
}

func DumpSuperBlock(cmd *cobra.Command, _ []string) error {

	const format = `
	Last mounted on:          %s
	Volume UUID:              %v
	Magic signature:          0x%x
	Revision level:           %d
	Features:                 %v
	State:                    %v
	Errors handling:          %v
	OS:                       %v
	Total inodes:             %d
	Free inodes:              %d
	Total blocks:             %d
	Free blocks:              %d
	First data block:         %d
	Block size:               %d
	Inode size:               %d
	Group descriptor size:    %d
	Reserved GDT blocks:      %d
	Blocks per group:         %d
	Clusters per group:       %d
	Inodes per group:         %d
	Creation time:            %v
	Mount time:               %v
	Write time:               %v
	Mount count:              %d
	Maximum mount count:      %d
	Last check time:          %v
	Check interval:           %d
	Default blocks uid:       %d
	Default blocks gid:       %d
	First free inode:         %d
	Min extra inode size:     %d
	Want extra inode size:    %d
	Journal inode:            %d
	Hash algorithm:           %v
	Checksum algorithm:       %v
	Checksum:                 0x%x
   	
`
	sb := fs.RawSuperBlock

	if sb != nil {
		cmd.Printf(format,
			sb.Info().MountedAt,
			sb.Info().ID,
			sb.Signature,
			sb.RevisionLevel,
			sb.Info().Features,
			sb.State,
			sb.ErrorHandling,
			sb.OS,
			sb.TotalInodes,
			sb.FreeInodes,
			sb.Info().TotalBlocks,
			sb.Info().FreeBlocks,
			sb.FirstDataBlockIdx,
			sb.Info().BlockSize,
			sb.InodeSize,
			sb.WantBlockGroupDescSize,
			sb.ReservedGDTBlocks,
			sb.BlocksPerGroup,
			sb.BlocksPerGroup,
			sb.InodesPerGroup,
			sb.Info().CreateTime,
			sb.MountTime,
			sb.WriteTime,
			sb.Mounts,
			sb.MaxMounts,
			sb.LastCheckTime,
			sb.CheckInterval,
			sb.DefaultGID,
			sb.DefaultUID,
			sb.FirstFreeInode,
			sb.WantExtraInodeSize,
			sb.NeedExtraInodeSize,
			sb.JournalInode,
			sb.HashAlgorithm,
			sb.ChecksumAlgorithm,
			sb.Checksum,
		)
	}

	return nil
}
