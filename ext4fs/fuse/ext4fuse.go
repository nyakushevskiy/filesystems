package main

import (
	"flag"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"

	"github.com/hanwen/go-fuse/fuse"
	"github.com/hanwen/go-fuse/fuse/nodefs"
	"github.com/hanwen/go-fuse/fuse/pathfs"
	"github.com/sirupsen/logrus"
	"github.com/spf13/pflag"

	"bitbucket.org/nyakushevskiy/filesystems/ext4fs/ext4"
)

type Ext4fs struct {
	*ext4.Ext4
	pathfs.FileSystem
	Mntpath string
}

func (efs *Ext4fs) String() string {
	return efs.Ext4.Type()
}

func (efs *Ext4fs) GetAttr(name string, context *fuse.Context) (*fuse.Attr, fuse.Status) {

	id := GetLogID()
	log.Debugf(`GetAttr [%.4x]: called for file /%s from {gid=%v, pid=%v, uid=%v}`,
		id, name, context.Gid, context.Pid, context.Uid)

	f, err := efs.OpenFile("/" + name)
	if ext4.IsNotFoundError(err) {
		log.Errorf(`GetAttr [%.4x]: file /%s not found!`, id, name)
		return nil, fuse.ENOENT
	} else if err != nil {
		log.Errorf(`GetAttr [%.4x]: open /%s failed: %v`, id, name, err)
		return nil, fuse.EAGAIN
	}

	attr := &fuse.Attr{
		Ino:       uint64(f.Inode().Num),
		Size:      uint64(f.Inode().DataSize),
		Blocks:    uint64(f.Inode().DataBlocks),
		Atime:     uint64(f.Inode().RawEntry.AccessTime),
		Mtime:     uint64(f.Inode().RawEntry.ModTime),
		Ctime:     uint64(f.Inode().RawEntry.CreationTime),
		Atimensec: uint32(f.Inode().RawEntry.AccessTimeExtraPrecision),
		Mtimensec: uint32(f.Inode().RawEntry.ModTimeExtraPrecision),
		Ctimensec: uint32(f.Inode().RawEntry.ChangeTimeExtraPrecision),
		Nlink:     uint32(f.Inode().RawEntry.HardLinks),
		Mode:      uint32(f.Inode().RawEntry.Mode),
		Owner: fuse.Owner{
			Gid: uint32(f.Inode().RawEntry.OwnerGidLow) | uint32(f.Inode().RawEntry.OwnerGidHigh)<<16,
			Uid: uint32(f.Inode().RawEntry.OwnerUidLow) | uint32(f.Inode().RawEntry.OwnerUidHigh)<<16,
		},
		Blksize: uint32(efs.BlockSize),
	}

	log.Debugf(`GetAttr [%.4x]: dump of attribute: %v`, id, attr)
	log.Infof(`GetAttr [%.4x]: success!`, id)

	return attr, fuse.OK
}

func (efs *Ext4fs) OpenDir(name string, context *fuse.Context) ([]fuse.DirEntry, fuse.Status) {

	id := GetLogID()
	log.Debugf(`OpenDir [%.4x]: called for file /%s from {gid=%v, pid=%v, uid=%v}`,
		id, name, context.Gid, context.Pid, context.Uid)

	dir, err := efs.OpenFile("/" + name)
	if ext4.IsNotFoundError(err) {
		log.Errorf(`OpenDir [%.4x]: file /%s not found!`, id, name)
		return nil, fuse.ENOENT
	} else if err != nil {
		log.Errorf(`OpenDir [%.4x]: open file /%s failed: %v`, id, name, err)
		return nil, fuse.EAGAIN
	}

	if !dir.IsDir() {
		log.Errorf(`OpenDir [%.4x]: file /%s is not directory!`, id, name)
		return nil, fuse.ENOTDIR
	}

	files, err := dir.ListFiles()
	if err != nil {
		log.Errorf(`OpenDir [%.4x]: list file /%s failed: %v`, id, name, err)
		return nil, fuse.EAGAIN
	}

	var dnames []string
	var dentries []fuse.DirEntry

	for i, f := range files {

		d := fuse.DirEntry{
			Ino:  uint64(f.Inode().Num),
			Mode: uint32(f.Inode().RawEntry.Mode),
			Name: filepath.Base(f.Name()),
		}

		log.Debugf(`OpenDir [%.4x]: dump of dir /%s entry #%v: %v`, id, name, i, d)

		dnames = append(dnames, f.Name())
		dentries = append(dentries, d)
	}

	log.Infof(`OpenDir [%.4x]: success!`, id)

	return dentries, fuse.OK
}

func (efs *Ext4fs) Open(name string, flags uint32, context *fuse.Context) (nodefs.File, fuse.Status) {

	id := GetLogID()
	log.Debugf(`Open [%.4x]: called for file /%s with flags %.8x from {gid=%v, pid=%v, uid=%v}`,
		id, name, flags, context.Gid, context.Pid, context.Uid)

	file, err := efs.OpenFile("/" + name)
	if ext4.IsNotFoundError(err) {
		log.Errorf(`Open [%.4x]: file /%s not found!`, id, name)
		return nil, fuse.ENOENT
	} else if err != nil {
		log.Errorf(`Open [%.4x]: open file /%s failed: %v`, id, name, err)
		return nil, fuse.EAGAIN
	}

	if !file.IsRegular() {
		log.Errorf(`Open [%.4x]: file /%s not found, only directory!`, id, name)
		return nil, fuse.ENOENT
	}

	if (flags & (syscall.O_WRONLY | syscall.O_RDWR)) != 0 {
		log.Errorf(`Open [%.4x]: invalid file access!`, id)
		return nil, fuse.EPERM
	}
	if (flags & syscall.O_RDONLY) != 0 {
		var read bool
		uid := uint32(file.Inode().RawEntry.OwnerUidLow) | uint32(file.Inode().RawEntry.OwnerUidHigh)<<16
		gid := uint32(file.Inode().RawEntry.OwnerGidLow) | uint32(file.Inode().RawEntry.OwnerGidHigh)<<16
		switch {
		case uid == context.Uid:
			read = (file.Inode().Mode & syscall.S_IRUSR) != 0
		case gid == context.Gid:
			read = (file.Inode().Mode & syscall.S_IRGRP) != 0
		default:
			read = (file.Inode().Mode & syscall.S_IROTH) != 0
		}
		if !read {
			log.Errorf(`Open [%.4x]: invalid file access!`, id)
			return nil, fuse.EPERM
		}
	}

	data, err := file.ReadAll()
	if err != nil {
		log.Errorf(`Open [%.4x]: read file /%s failed: %v`, id, name, err)
		return nil, fuse.EAGAIN
	}

	var f nodefs.File
	f = nodefs.NewDataFile(data)
	f = nodefs.NewReadOnlyFile(f)

	log.Infof(`Open [%.4x]: success!`, id)

	return f, fuse.OK
}

var (
	logid uint16
	log   = logrus.New()
)

func GetLogID() uint16 {
	logid++
	return logid
}

var (
	mntpath = pflag.StringP(`mnt-path`, `m`, os.Getenv(`EXT4FS_MNT`), `mount point of the ext4 fs`)
	devpath = pflag.StringP(`dev-path`, `d`, os.Getenv(`EXT4FS_DEV`), `path to device formatted with ext4 fs`)
	logpath = pflag.StringP(`log-path`, `l`, os.Getenv(`EXT4FS_LOG`), `path to log file`)
)

func main() {

	var err error

	pflag.Parse()
	if *mntpath == `` {
		log.Fatalf(`Flag '--mount-point' or '-m' is not set: %v`, flag.ErrHelp)
	}
	if *devpath == `` {
		log.Fatalf(`Flag '--dev-path' or '-d' is not set: %v`, flag.ErrHelp)
	}

	var logf io.Writer
	if *logpath != `` {
		logf, err = os.Create(*logpath)
		if err != nil {
			log.Fatal(`Create %s fail: %v`, logpath, err)
		}
	} else {
		logf = ioutil.Discard
	}

	devf, err := os.Open(*devpath)
	if err != nil {
		log.Fatalf(`Open %s fail: %v`, err)
	}

	efs, err := ext4.Open(devf)
	if err != nil {
		log.Fatalf(`Open ext4 fs fail: %v`, err)
	}

	var fs pathfs.FileSystem
	fs = pathfs.NewDefaultFileSystem()
	fs = pathfs.NewReadonlyFileSystem(fs)

	nfs := pathfs.NewPathNodeFs(
		&Ext4fs{Ext4: efs, FileSystem: pathfs.NewDefaultFileSystem()},
		&pathfs.PathNodeFsOptions{Debug: true},
	)

	server, _, err := nodefs.MountRoot(*mntpath, nfs.Root(), nil)
	if err != nil {
		log.Fatalf(`Mount fail: %v`, err)
	}

	log.Out = logf
	log.SetLevel(logrus.DebugLevel)

	server.Serve()
}
