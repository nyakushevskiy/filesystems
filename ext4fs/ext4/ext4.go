package ext4

import (
	"fmt"
	"io"
	"time"

	"github.com/satori/go.uuid"
)

// Ext4 represents an instance of EXT file system (ignoring the version).
type Ext4 struct {
	rd   io.ReaderAt // reader of this device
	root *File       // root of ext4 fs

	IsOpened      bool        // if Open() method was called
	RawSuperBlock *SuperBlock // raw super block on disk structure

	/* collected info from the super block, if it was read */

	// avaliable features:
	HasMetaGroups         bool
	HasFlexGroups         bool
	HasSparseMetadata     bool
	HasChecksums          bool
	HasGroupDescChecksums bool

	// blocks stat:
	BlockSize         int64
	BlocksPerMetadata int64
	BlocksPerGroup    int64
	Blocks            int64

	// block groups stat:
	GroupSize          int64
	GroupDescSize      int64
	GroupsPerMetaGroup int64
	Groups             int64

	// inodes stat:
	InodeSize      int64
	InodesPerGroup int64
	Inodes         int64

	// for checksums:
	UUID         uuid.UUID
	ChecksumSeed uint32
}

// New returns new instance of fs, but doesn't open it.
func New(rd io.ReaderAt) *Ext4 {
	return &Ext4{rd: rd}
}

// Open returns a new instance of opened file system and error, if happened.
func Open(rd io.ReaderAt) (*Ext4, error) {
	fs := New(rd)
	return fs, fs.OpenSelf()
}

// Type returns type (like EXT4, NTFS...) of this file system.
func (fs *Ext4) Type() string {
	return Type
}

// Size returns size of this file system in bytes.
func (fs *Ext4) Size() int64 {
	return fs.BlockSize * fs.Blocks
}

// ReadAt implements io.ReaderAt ReadAt() method.
func (fs *Ext4) ReadAt(p []byte, off int64) (int, error) {
	return fs.rd.ReadAt(p, off)
}

// ReadBlockAt reads n blocks at loc.
func (fs *Ext4) ReadBlocksAt(loc, n int64) (data []byte, err error) {

	if loc < 0 || loc >= fs.Blocks {
		return nil, fmt.Errorf("ReadBlocksAt: no such block as <%d>", loc)
	}

	if fs.IsOpened {
		data = make([]byte, n*fs.BlockSize)
		_, err = io.ReadFull(io.NewSectionReader(fs.rd, loc*fs.BlockSize, n*fs.BlockSize), data)
	}

	return data, err
}

// initBy initalizes fs by properties of sb.
func (fs *Ext4) initBy(sb *SuperBlock) {

	fs.IsOpened = true
	fs.RawSuperBlock = sb

	// should collect all needed info from superblock

	fs.HasMetaGroups = sb.IncompatFeatures&INCOMPAT_META_BG != 0
	fs.HasFlexGroups = sb.IncompatFeatures&INCOMPAT_FLEX_BG != 0
	fs.HasSparseMetadata = sb.RoCompatFeatures&RO_COMPAT_SPARSE_SUPER != 0
	fs.HasChecksums = sb.RoCompatFeatures&RO_COMPAT_METADATA_CSUM != 0
	fs.HasGroupDescChecksums = sb.RoCompatFeatures&RO_COMPAT_GDT_CSUM != 0

	fs.BlockSize = int64(1 << (10 + sb.LogExtraBlockSize))
	fs.BlocksPerGroup = int64(sb.BlocksPerGroup)
	fs.Blocks = int64(sb.TotalBlocksLow) | int64(sb.TotalBlocksHigh)<<32

	fs.GroupSize = fs.BlocksPerGroup * fs.BlockSize
	if sb.IncompatFeatures&INCOMPAT_64BIT != 0 {
		fs.GroupDescSize = int64(sb.WantBlockGroupDescSize)
	} else {
		fs.GroupDescSize = 32
	}
	fs.GroupsPerMetaGroup = fs.BlockSize / fs.GroupDescSize
	fs.Groups = (fs.Blocks + fs.BlocksPerGroup - 1) / fs.BlocksPerGroup

	if fs.HasMetaGroups {
		fs.BlocksPerMetadata = 2
	} else {
		fs.BlocksPerMetadata = 1 + ((fs.GroupDescSize*fs.Groups + fs.BlockSize - 1) / fs.BlockSize)
	}

	fs.InodeSize = int64(sb.InodeSize)
	fs.InodesPerGroup = int64(sb.InodesPerGroup)
	fs.Inodes = fs.Groups * fs.InodesPerGroup

	if fs.HasChecksums {
		fs.UUID = sb.VolumeUUID
		if sb.IncompatFeatures&INCOMPAT_CSUM_SEED != 0 {
			fs.ChecksumSeed = sb.WantChecksumSeed
		} else {
			fs.ChecksumSeed = InitCrc32c(fs.UUID.Bytes())
		}
	}
}

// verify verifies checksum of sb, if fs has meta_csum feature enabled.
func (fs *Ext4) verify(sb *SuperBlock) error {

	if !fs.HasChecksums {
		return nil
	}

	data := sb.Bytes()
	if len(data) > 4 { // size of checksum is 4
		data = data[:len(data)-4]
	}

	// checksum of super block is calculated as src32c of raw super block up to the checksum field
	calc := Crc32c(data)

	return VerifyFormatOf(SuperBlockType, "checksum", sb.Checksum, calc)
}

// PrimarySuperBlock returns primary superblock of fs, located in block group 0.
func (fs *Ext4) PrimarySuperBlock() (*SuperBlock, error) {

	const PrimarySuperBlockOff = 1024

	sb := new(SuperBlock)

	// primary superblock doesn't rely on any fs features,
	// and has to be located just after the boot code
	err := sb.ReadFrom(io.NewSectionReader(fs, PrimarySuperBlockOff, SuperBlockSize))
	if err != nil {
		return nil, err
	}

	// not verifying the right checksum value
	return sb, nil
}

const MaxFails = 3

// OpenSelf is method of late initialization,
// that opens file system, if it wasn't open before.
func (fs *Ext4) OpenSelf() (err error) {

	if fs.IsOpened {
		return nil
	}

	var m int64 // number of a block group to take a superblock from
	var sb *SuperBlock

	var verifyErr error

	for fails := 0; (fails < MaxFails) && (m == 0 || m < fs.Groups); fails++ {

		var metagroup *BlockGroup

		if m == 0 {
			sb, err = fs.PrimarySuperBlock()
			if err != nil {
				break
			}
			fs.initBy(sb)

		} else {
			metagroup, err = fs.BlockGroup(m)
			if err != nil {
				break
			}
			sb, err = metagroup.SuperBlock()
			if err != nil {
				break
			}
		}

		verifyErr = fs.verify(sb)
		if verifyErr == nil {
			break
		}

		m++

		for !fs.HasMetadataInBlockGroup(m) {
			if m++; m >= fs.Groups {
				break
			}
		}
	}

	// if verify error was catched,
	// then other errors are considered to be caused by corruption of data
	if verifyErr != nil {
		err = verifyErr
	}

	return err
}

// Info describes a file system and is returned by Stat() method of VirtFileSystem.
type Info struct {
	Type        string
	ID          string
	MountedAt   string
	CreateTime  time.Time
	Features    []string
	TotalBlocks int64
	FreeBlocks  int64
	BlockSize   int
}

// Stat returns Info structure describing this file system.
func (fs *Ext4) Stat() (*Info, error) {

	err := fs.OpenSelf()
	if err != nil {
		return nil, err
	}

	return fs.RawSuperBlock.Info(), err
}
