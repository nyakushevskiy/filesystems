package ext4

import (
	"bufio"
	"bytes"
	"io"
	"unsafe"
)

// BlockMap represents a structure in inode's iblock, if EXTENTS_FL flag is set.
type ExtentTree struct {
	fs     *Ext4  // fs of this inode
	inode  *Inode // inode associated with this extent tree
	IsRoot bool   // is root of extent tree, located in inode

	/* info collected from inode entry, if it was read */
	Depth   int              // depth of tree
	Entries []unsafe.Pointer // if depth == 0, then direct entries, else indirect
}

// ExtentTree parses an extent tree root from inode's iblock and returns it.
func (inode *Inode) ExtentTree() (*ExtentTree, error) {
	tree := &ExtentTree{fs: inode.fs, inode: inode, IsRoot: true}
	return tree.from(inode.Iblock)
}

// ChildBlockMap parses extent tree node
// from block with n location of parent and returns it.
func (parent *ExtentTree) ChildExtentTree(n int) (*ExtentTree, error) {

	entry := (*IndirectExtentTreeEntry)(parent.Entries[n])

	tree := &ExtentTree{fs: parent.fs, inode: parent.inode}

	loc := int64(entry.ChildNodeLocationLow) | int64(entry.ChildNodeLocationHigh)<<32
	data, err := tree.fs.ReadBlocksAt(loc, 1)
	if err != nil {
		return nil, err
	}

	return tree.from(data)
}

// initBy initializes tree by properties of node.
func (tree *ExtentTree) initBy(node *ExtentTreeNode) {
	tree.Depth = int(node.Header.Depth)
	tree.Entries = node.Entries
}

// verify verifies checksum of node,
// if fs has meta_csum feature enabled and it's not a root of a tree.
func (tree *ExtentTree) verify(node *ExtentTreeNode) error {

	// inode checksum was calculated, so inode map doesn't have another one checksum
	if !tree.fs.HasChecksums || tree.IsRoot {
		return nil
	}

	data := node.Bytes()
	if len(data) > 4 { // sizeof(checksum) == 4
		data = data[:len(data)-4]
	}

	// checksum of extent tree node is calculated as src32c of:
	// UUID + inode number + inode generation + raw extent tree node up to the checksum field
	calc := FinalizeCrc32c(tree.inode.ChecksumSeed, data)

	return VerifyFormatOf(ExtentTreeType, "checksum", node.Checksum, calc)
}

// from parses tree from b.
func (tree *ExtentTree) from(b []byte) (*ExtentTree, error) {

	node := new(ExtentTreeNode)

	err := node.ReadFrom(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	tree.initBy(node)

	return tree, tree.verify(node)
}

// IsLeaf reports whether this tree entries points directly to extents.
func (tree *ExtentTree) IsLeaf() bool {
	return tree.Depth == 0
}

// Size returns the size of underlying data.
func (tree *ExtentTree) Size() int64 {
	return tree.inode.DataSize
}

// DataBlockIter returns an iterator of blocks in tree.
// which reads blocks, while iterating through them.
func (tree *ExtentTree) DataBlockIter() *ExtentTreeIter {
	return &ExtentTreeIter{tree: tree, data: true}
}

// BlockIter returns an iterator of blocks in tree,
// which doesn't read blocks, while iterating through them.
func (tree *ExtentTree) BlockIter() *ExtentTreeIter {
	return &ExtentTreeIter{tree: tree, data: false}
}

// BlockMapIter is an iterator of blocks in a block map.
type ExtentTreeIter struct {
	tree  *ExtentTree // tree to iterate blocks
	data  bool        // if true, iterating blocks, but not reading data, only locations
	pos   int         // index of current tree entry
	leaf  *ExtentIter // child extent tree iter if depth > 0, otherwise it's an extent
	index *ExtentTreeIter
}

// Block implements Block() method of BlockIter.
func (iter *ExtentTreeIter) Block() ([]byte, int64) {
	if iter.tree.IsLeaf() {
		return iter.leaf.Block()
	} else {
		return iter.index.Block()
	}
}

// Next implements Next() method of BlockIter.
func (iter *ExtentTreeIter) Next() error {
	if iter.tree.IsLeaf() {
		return iter.nextInLeaf()
	} else {
		return iter.nextInIndex()
	}
}

// nextInLeaf is used in extent tree leaf node iterators (depth = 0),
// tries to find next block and read it, if neccessary.
func (iter *ExtentTreeIter) nextInLeaf() (err error) {

	if iter.leaf != nil {

		err = iter.leaf.Next()
		if err != EOB {
			return err
		}

		iter.pos++
		if iter.pos >= len(iter.tree.Entries) {
			return EOB
		}
	}

	extent := iter.tree.ChildExtent(iter.pos)
	if iter.data {
		iter.leaf = extent.DataBlockIter()
	} else {
		iter.leaf = extent.BlockIter()
	}

	return iter.leaf.Next()
}

// nextInLeaf is used in extent tree index node iterators (depth = 0),
// tries to find the next block and read it, if neccessary.
func (iter *ExtentTreeIter) nextInIndex() (err error) {

	if iter.index != nil {

		err = iter.index.Next()
		if err != EOB {
			return err
		}

		iter.pos++
		if iter.pos >= len(iter.tree.Entries) {
			return EOB
		}
	}

	tree, err := iter.tree.ChildExtentTree(iter.pos)
	if err != nil {
		return err
	}

	if iter.data {
		iter.index = tree.DataBlockIter()
	} else {
		iter.index = tree.BlockIter()
	}

	return iter.index.Next()
}

// Extent represents an extent.
type Extent struct {
	rd       io.Reader
	bs       int64
	Location int64 // numer of blocks to start of this extent
	Blocks   int64 // number of blocks in this extent
}

// ChildExtent returns an extent of fs created by n parent entry.
func (parent *ExtentTree) ChildExtent(n int) *Extent {

	entry := (*DirectExtentTreeEntry)(parent.Entries[n])

	var (
		location = int64(entry.ExtentLocationLow) | int64(entry.ExtentLocationHigh)<<32
		blocks   = int64(entry.BlocksPerExtent)
	)

	extent := &Extent{nil, parent.fs.BlockSize, location, blocks}

	if extent.IsAllocated() {
		extent.rd = io.NewSectionReader(parent.fs, extent.Location*extent.bs, extent.Blocks*extent.bs)
		extent.rd = bufio.NewReaderSize(extent.rd, int(extent.bs))
	} else {
		extent.rd = DevZero
	}

	return extent
}

// ReadBlock reads next block from extent.
func (extent *Extent) ReadBlock() (data []byte, err error) {

	data = make([]byte, extent.bs)
	_, err = io.ReadFull(extent.rd, data)

	return data, err
}

// IsAllocated reports wheter extent is allocated.
func (extent *Extent) IsAllocated() bool {
	return extent.Location > 0
}

// Size returns the size of underlying data.
func (extent *Extent) Size() int64 {
	return extent.Blocks * extent.bs
}

// DataBlockIter returns an iterator of extent,
// which reads blocks, while iterating through them.
func (extent *Extent) DataBlockIter() *ExtentIter {
	return &ExtentIter{extent: extent, read: true}
}

// BlockIter returns an iterator of extent,
// which doesn't read blocks, while iterating through them.
func (extent *Extent) BlockIter() *ExtentIter {
	return &ExtentIter{extent: extent, read: false}
}

// ExtentIter is an iterator of an extent.
type ExtentIter struct {
	extent   *Extent
	read     bool
	location int64
	data     []byte
}

// Block implements Block() method of BlockIter
func (iter *ExtentIter) Block() ([]byte, int64) {
	return iter.data, iter.location
}

// Next implements Next() method of BlockIter
func (iter *ExtentIter) Next() (err error) {

	if iter == nil {
		return EOB
	}

	if iter.location <= 0 {
		iter.location = iter.extent.Location
	} else {
		iter.location++
	}

	if iter.read {
		iter.data, err = iter.extent.ReadBlock()
		if err == io.EOF {
			return EOB
		}
	}

	return err
}
