package ext4

import (
	"fmt"
	"path/filepath"
	"reflect"
	"strings"
)

// DevZero is a device full of zeros.
var DevZero = devZero{}

type devZero struct{}

// Read implements Read() method of io.Reader.
func (zero devZero) Read(p []byte) (int, error) {
	for i := range p {
		p[i] = 0
	}
	return len(p), nil
}

// ReadAt implements ReadAt() method of io.ReaderAt.
func (zero devZero) ReadAt(p []byte, _ int64) (int, error) {
	return zero.Read(p)
}

// Write implements Write() method of io.Writer.
func (zero devZero) Write(p []byte) (int, error) {
	return len(p), nil
}

// WriteAt implements WriteAt() method of io.WriterAt.
func (zero devZero) WriteAt(p []byte, _ int64) (int, error) {
	return zero.Write(p)
}

// SplitPath split path into the list of file names.
func SplitPath(path string) []string {
	path = filepath.Clean(path)
	path = strings.TrimSuffix(path, "/")
	path = strings.TrimPrefix(path, "/")
	return strings.Split(path, "/")
}

// FlagNames returns names of active flags.
func FlagNames(flags uint64, names []string) []string {

	var setnames []string

	for shift, name := range names {
		if flags&(1<<uint(shift)) != 0 {
			setnames = append(setnames, name)
		}
	}

	return setnames
}

type FormatError struct {
	error
}

func IsFormatError(err error) bool {
	_, ok := err.(FormatError)
	return ok
}

func VerifyFormatOf(what, by string, got, expected interface{}) error {
	if !reflect.DeepEqual(got, expected) {
		return FormatError{fmt.Errorf("couldn't verify format of %v by %v (got %v, expected %v)",
			what, by, got, expected)}
	}
	return nil
}

type NotFoundError struct {
	error
}

func IsNotFoundError(err error) bool {
	_, ok := err.(FormatError)
	return ok
}

func NotFound(dir, file string) error {
	return NotFoundError{fmt.Errorf("file %q was not found in %q", file, dir)}
}
