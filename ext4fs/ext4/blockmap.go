package ext4

import (
	"bytes"
	"encoding/binary"
)

// ForEach iterates bi and calls f for every block.
func ForEachBlock(bi BlockIter, f func(data []byte, location int64) bool) (err error) {

	if bi == nil {
		return nil
	}

	for err = bi.Next(); err == nil; err = bi.Next() {
		if !f(bi.Block()) {
			break
		}
	}

	if err == EOB {
		return nil
	} else {
		return err
	}
}

// BlockMap represents a structures in inode's iblock, if EXTENTS_FL flag is not set.
type BlockMap struct {
	fs     *Ext4     // file system of block map
	next   *BlockMap // if it's a root, then it's next block map in order (direct -> indirect -> double-indirect -> triple-indirect)
	size   int64     // size of underlying data
	Blocks int64     // total number of mapped blocks
	Level  int       // level of block map (0 == direct locations, 1 == locations of block, storing direct locations, etc.)

	/* info collected from map */
	Locations []uint32
}

// BlockMap parses a block map from inode's iblock and returns it.
func (inode *Inode) BlockMap() (*BlockMap, error) {

	blkmap := &BlockMap{fs: inode.fs, size: inode.DataSize, Level: 0, Blocks: inode.DataBlocks}

	iblock := bytes.NewReader(inode.Iblock)

	/* first 12 uint32 of inode iblock is direct locations to file blocks */
	blocks, mapping := int64(12), int64(1) // mapping 1 : 1

	blkmap.Locations = make([]uint32, blocks)
	binary.Read(iblock, binary.LittleEndian, &blkmap.Locations)

	last := blkmap

	/* then it goes 3 uint32, which is indirect, double-indirect, triple-indirect locations of blocks */
	for level := 0; (level < 3) && (blocks < blkmap.Blocks); level++ {

		blkmap := &BlockMap{fs: inode.fs, Level: level, Blocks: inode.DataBlocks}

		var loc uint32
		binary.Read(iblock, binary.LittleEndian, &loc)

		data, err := blkmap.fs.ReadBlocksAt(int64(loc), 1)
		if err != nil {
			return nil, err
		}

		blkmap.Locations = make([]uint32, len(data)/4)
		binary.Read(bytes.NewReader(data), binary.LittleEndian, blkmap.Locations)

		mapping *= int64(len(data) / 4) // mapping 1 : (len(data) / 4) ^ level
		blocks += mapping

		last.next = blkmap
		last = blkmap
	}

	return blkmap, nil
}

// ChildBlockMap parses block map
// from block with n location of parent and returns it.
func (parent *BlockMap) ChildBlockMap(n int) (*BlockMap, error) {

	if parent.IsDirect() {
		return nil, nil
	}

	blkmap := &BlockMap{fs: parent.fs, Level: parent.Level - 1, Blocks: parent.Blocks}

	loc := int64(parent.Locations[n])
	data, err := blkmap.fs.ReadBlocksAt(loc, 1)
	if err != nil {
		return nil, err
	}

	blkmap.Locations = make([]uint32, len(data)/4)
	binary.Read(bytes.NewReader(data), binary.LittleEndian, blkmap.Locations)

	return blkmap, nil
}

// IsDirect reports whether block map locations points directly to data blocks.
func (blkmap *BlockMap) IsDirect() bool {
	return blkmap.Level == 0
}

// Size returns the size of underlying data
func (blkmap *BlockMap) Size() int64 {
	return blkmap.size
}

// DataBlockIter returns an iterator of blocks in blkmap.
// which reads blocks, while iterating through them.
func (blkmap *BlockMap) DataBlockIter() *BlockMapIter {
	return &BlockMapIter{blkmap: blkmap, read: true}
}

// BlockIter returns an iterator of blocks in blkmap,
// which doesn't read blocks, while iterating through them.
func (blkmap *BlockMap) BlockIter() *BlockMapIter {
	return &BlockMapIter{blkmap: blkmap, read: false}
}

// BlockMapIter is an iterator of blocks in a block map.
type BlockMapIter struct {
	blkmap   *BlockMap     // block map to iterate data blocks
	read     bool          // reports whether reading block's data is required
	lpos     int           // current location number
	bpos     int64         // current block number
	location int64         // if block map is direct, then location of block
	data     []byte        // if block map is direct, then block data
	child    *BlockMapIter // if block map is indirect, then child block map iterator
}

// Block implements Block() method of fs.VirtBlockIter.
func (iter *BlockMapIter) Block() ([]byte, int64) {
	if iter.blkmap.IsDirect() {
		return iter.data, iter.location
	} else {
		return iter.child.Block()
	}
}

// Next implements Next() method of fs.VirtBlockIter.
func (iter *BlockMapIter) Next() (err error) {

	if iter.blkmap == nil || iter.bpos >= iter.blkmap.Blocks {
		return EOB
	}

	if iter.blkmap.IsDirect() {
		err = iter.nextInDirect()
	} else {
		err = iter.nextInIndirect()
	}
	if err != EOB {
		iter.bpos++
		return err
	}

	iter.lpos = 0
	iter.blkmap = iter.blkmap.next

	return iter.Next()
}

// nextInDirect is used in direct block map iterators (level = 0),
// tries to find next block and read it, if neccessary.
func (iter *BlockMapIter) nextInDirect() (err error) {

	if iter.lpos >= len(iter.blkmap.Locations) {
		return EOB
	}

	iter.location = int64(iter.blkmap.Locations[iter.lpos])
	iter.lpos++

	if iter.read {
		if iter.location > 0 {
			iter.data, err = iter.blkmap.fs.ReadBlocksAt(iter.location, 1)
		} else {
			iter.data = make([]byte, iter.blkmap.fs.BlockSize)
		}
	}

	return err
}

// nextInIndirect is used in indirect block map iterators (level > 0),
// tries to find the next block and read it, if neccessary.
func (iter *BlockMapIter) nextInIndirect() error {

	if iter.child != nil {

		err := iter.child.Next()
		if err != EOB {
			return err
		}

		iter.lpos++
		if iter.lpos >= len(iter.blkmap.Locations) {
			return EOB
		}
	}

	blkmap, err := iter.blkmap.ChildBlockMap(iter.lpos)
	if err != nil {
		return err
	}

	if iter.read {
		iter.child = blkmap.DataBlockIter()
	} else {
		iter.child = blkmap.BlockIter()
	}

	return iter.child.Next()
}
