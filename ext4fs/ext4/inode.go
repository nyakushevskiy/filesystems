package ext4

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"
	"unsafe"
)

// Inode represents an inode.
type Inode struct {
	fs    *Ext4       // fs of this inode
	group *BlockGroup // group of this inode

	Num       int64       // number of this inode (start from 0)
	RawEntry  *InodeEntry // raw inode on disk structure
	RawSuffix []byte      // raw inode suffix

	/* collected info from the inode entry */

	// avaliable features:
	HasExtentTree bool
	HasHashTree   bool
	HasInlineData bool

	// file stat:
	DataSize int64
	ModTime  time.Time
	Mode     os.FileMode

	// for mapping file data blocks:
	Iblock        []byte
	InlineData    []byte
	XattrLocation int64
	DataBlocks    int64

	// for checksums:
	ChecksumSeed uint32
}

// Inode returns n inode of fs (inode with number 0 doesn't exists).
func (fs *Ext4) Inode(n int64) (*Inode, error) {

	err := fs.OpenSelf()
	if err != nil {
		return nil, err
	}

	if n < 1 || n > fs.Inodes {
		return nil, fmt.Errorf("Inode: no such inode as <%d>", n)
	}

	group, err := fs.BlockGroup((n - 1) / fs.InodesPerGroup)
	if err != nil {
		return nil, err
	}

	return group.Inode((n - 1) % fs.InodesPerGroup)
}

// Inode returns n inode of group.
func (group *BlockGroup) Inode(n int64) (*Inode, error) {

	if n < 0 || n >= group.fs.InodesPerGroup {
		return nil, fmt.Errorf("Inode: no such inode as <%d> in block group <%d>", n, group.Num)
	}

	inode := &Inode{fs: group.fs, group: group}
	inode.Num = group.fs.InodesPerGroup*group.Num + n + 1

	entry := new(InodeEntry)

	off := group.fs.BlockSize*group.InodeTableLocation + group.fs.InodeSize*n
	rd := io.NewSectionReader(group.fs, off, inode.fs.InodeSize)

	err := entry.ReadFrom(rd)
	if err != nil {
		return nil, err
	}

	suffix, err := ioutil.ReadAll(rd) // read up to the fs.InodeSize to verify checksum
	if err != nil {
		return nil, err
	}

	inode.initBy(entry, suffix)

	return inode, inode.verify(entry, suffix)
}

// initBy initializes inode by properties from entry and suffix.
func (inode *Inode) initBy(entry *InodeEntry, suffix []byte) {

	bs, is := inode.fs.BlockSize, inode.fs.InodeSize

	inode.RawEntry = entry
	inode.RawSuffix = suffix

	inode.HasExtentTree = entry.Flags&EXTENTS_FL != 0
	inode.HasHashTree = entry.Flags&INDEX_FL != 0
	inode.HasInlineData = entry.Flags&INLINE_DATA_FL != 0

	inode.DataSize = int64(entry.SizeHigh)<<32 | int64(entry.SizeLow)
	inode.Mode = entry.Mode.Os()

	modAt := Time64(entry.ModTime)
	if unsafe.Offsetof(entry.ModTime)+4 <= uintptr(is) {
		modAt.SetExtraPrecision(entry.ModTimeExtraPrecision)
	}
	inode.ModTime = modAt.Time()

	inode.Iblock = entry.Iblock[:]

	// number of bytes, which is mapped by inode data blocks
	n := inode.DataSize

	inode.XattrLocation = int64(entry.XattrLocationLow) | int64(entry.XattrLocationHigh)<<32
	if inode.HasInlineData {

		inode.InlineData = make([]byte, len(inode.Iblock))
		copy(inode.InlineData, inode.Iblock)

		if inode.DataSize > int64(len(inode.Iblock)) {
			// if the extended attributes feature is enabled and
			// an inode data size is more than could be stored in iblock,
			// than should also seach data in extended attributes
			xattrs, _ := inode.ListXattr()
			inode.InlineData = append(inode.InlineData, xattrs["system.data"]...)
		}

		n -= int64(len(inode.InlineData))
	}

	inode.DataBlocks = (n + bs - 1) / bs

	if inode.fs.HasChecksums {

		buf := new(bytes.Buffer)

		binary.Write(buf, binary.LittleEndian, uint32(inode.Num))
		binary.Write(buf, binary.LittleEndian, entry.Generation)

		inode.ChecksumSeed = UpdateCrc32c(inode.fs.ChecksumSeed, buf.Bytes())
	}
}

// verify verifies checksum of entry and suffix, if fs has meta_csum feature enabled.
func (inode *Inode) verify(entry *InodeEntry, suffix []byte) error {

	if !inode.fs.HasChecksums {
		return nil
	}

	is := inode.fs.InodeSize
	csumlo, csumhi := uint32(entry.ChecksumLow), uint32(entry.ChecksumHigh)

	csum := csumlo
	if unsafe.Offsetof(entry.ChecksumHigh)+4 <= uintptr(is) {
		csum |= csumhi << 16
	}

	// checksum of inode is calculated as src32c of:
	// UUID + inode number + inode generation + raw inode (with checksums set to 0x0)
	entry.ChecksumLow, entry.ChecksumHigh = 0, 0
	calc := FinalizeCrc32c(inode.ChecksumSeed, append(entry.Bytes(), suffix...))

	entry.ChecksumLow, entry.ChecksumHigh = uint16(csumlo), uint16(csumhi)
	return VerifyFormatOf(InodeEntryType, "checksum", csum, calc)
}

// EOB is a signal that VirtBlockIter has riched the end of blocks.
var EOB = errors.New("EOB")

// BlockIter is the common interface implemented by objects,
// which can iterate blocks.
type BlockIter interface {

	// Block returns data and location of block,
	// if IsEmpty() method returns false, otherwise the behaviour is undefined.
	Block() (data []byte, location int64)

	// FindNext tries to find the next block.
	// If iterator has riched the end of blocks, the error would be EOB.
	Next() (err error)
}

// BlockIter returns an iterator of blocks,
// which doesn't read blocks, while iterating through them.
func (inode *Inode) BlockIter() BlockIter {

	if inode.HasExtentTree {

		tree, err := inode.ExtentTree()
		if err == nil {
			return tree.BlockIter()
		}

	} else {

		blkmap, err := inode.BlockMap()
		if err == nil {
			return blkmap.BlockIter()
		}
	}

	return nil
}

// DataBlockIter returns an iterator of blocks,
// which reads blocks, while iterating through them.
func (inode *Inode) DataBlockIter() BlockIter {

	if inode.HasExtentTree {

		tree, err := inode.ExtentTree()
		if err == nil {
			return tree.DataBlockIter()
		}

	} else {

		blkmap, err := inode.BlockMap()
		if err == nil {
			return blkmap.DataBlockIter()
		}
	}

	return nil
}
