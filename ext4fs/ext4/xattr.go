package ext4

import (
	"bytes"
	"io"
	"io/ioutil"
)

// ListXattr lists extended attibutes of inode.
func (inode *Inode) ListXattr() (map[string][]byte, error) {

	var firstErr error
	xattrs := make(map[string][]byte)

	xattrInode, err := inode.listXattrInInode()
	if err != nil {
		firstErr = err
	} else {
		for name, value := range xattrInode {
			xattrs[name] = value
		}
	}

	xattrBlock, err := inode.listXattrInBlock()
	if firstErr == nil && err != nil {
		firstErr = err
	} else {
		for name, value := range xattrBlock {
			xattrs[name] = value
		}
	}

	return xattrs, firstErr
}

// listXattrInInode lists extended attibutes, located in inode suffix.
func (inode *Inode) listXattrInInode() (map[string][]byte, error) {

	xattrs := map[string][]byte{}

	rd := bytes.NewReader(inode.RawSuffix)

	var err error
	var xahead XattrShortHeader

	// position of an extended attribute header in an inode suffix is not known for sure,
	// so trying to read a header, until we find the right signature or EOF
	var off int64
	for {
		off += XattrShortHeaderSize
		err = xahead.ReadFrom(rd)
		if !IsFormatError(err) {
			break
		}
	}
	// should consider this case as an empty list of attributes
	if err == io.EOF {
		return xattrs, nil
	}
	if err != nil {
		return xattrs, err
	}

	// rd.ReadAt now reads with offset in bytes relative to the start of the first extended attribute entry,
	// this offset is specified in ValueOff field of each entry located in inode
	rd = bytes.NewReader(inode.RawSuffix[off:])

	return inode.readXattrsFrom(rd)
}

// listXattrInBlock lists extended attibutes, located in special block, specified in inode.
func (inode *Inode) listXattrInBlock() (map[string][]byte, error) {

	xattrs := map[string][]byte{}

	if inode.XattrLocation == 0 {
		return xattrs, nil
	}

	data, err := inode.fs.ReadBlocksAt(inode.XattrLocation, 1)
	if err != nil {
		return xattrs, err
	}

	// rd.ReadAt reads with offset in bytes relative to the start of the extended attribute block,
	// this offset is specified in ValueOff field of each entry located in block
	rd := bytes.NewReader(data)

	var xahead XattrLongHeader
	err = xahead.ReadFrom(rd)
	if IsFormatError(err) {
		return xattrs, nil
	}
	if err != nil {
		return xattrs, err
	}

	// it could be more, than one block in extended attribute block,
	// so if it is so, reading all blocks and skipping the header
	if int64(xahead.Blocks) > 1 {
		data, err = inode.fs.ReadBlocksAt(inode.XattrLocation, int64(xahead.Blocks))
		if err != nil {
			return nil, err
		}
		rd = bytes.NewReader(data)
		io.CopyN(DevZero, rd, XattrLongHeaderSize)
	}

	return inode.readXattrsFrom(rd)
}

// readsXattrsFrom reads list of extended attribute from rd.
func (inode *Inode) readXattrsFrom(rd *bytes.Reader) (map[string][]byte, error) {

	var err error
	xattrs := make(map[string][]byte)

	for {
		var xattr XattrEntry
		err = xattr.ReadFrom(rd)
		if err != nil {
			break
		}
		// if first three fields is zeroed, than it's the last entry
		if xattr.NameLen == 0 && xattr.NameIndex == 0 && xattr.ValueOff == 0 {
			break
		}
		// it means, that this value stored in ea_inode, which is not supported feature,
		// so just pretending, that we didn't find it
		if xattr.ValueInodeNum != 0 {
			continue
		}
		xattr.Value, err = ioutil.ReadAll(io.NewSectionReader(rd, int64(xattr.ValueOff), int64(xattr.ValueSize)))
		if err != nil {
			break
		}
		xattrs[xattr.Name] = xattr.Value
	}

	return xattrs, err
}
