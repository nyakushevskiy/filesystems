package ext4

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"unsafe"
)

// BlockGroup represents a series of block, in which file system is splited.
type BlockGroup struct {
	fs *Ext4 // fs of this block group

	Num         int64           // number of this block group, starts from 0
	Location    int64           // number of blocks to the start of this block group
	HasMetadata bool            // if this block group contains a copy of metadata, it's true
	RawDesc     *BlockGroupDesc // raw block group descriptor on disk structure

	/* collected info from the block group descriptor */

	// block bitmap info:
	BlockBitmapLocation int64  // number of blocks to the start of the block bitmap
	BlockBitmapChecksum uint32 // checksum of the block bitmap
	BlockBitmapCmask    uint32 // mask of significant bits in the block bitmap checksum
	BlockBitmapIsEmpty  bool   // if the block bitmap is empty, than it's true

	// inode bitmap info:
	InodeBitmapLocation int64
	InodeBitmapChecksum uint32
	InodeBitmapCmask    uint32
	InodeBitmapIsEmpty  bool

	// inode table info:
	InodeTableLocation int64
	InodeTableIsEmpty  bool
}

// BlockGroup returns n block group of fs.
func (fs *Ext4) BlockGroup(n int64) (*BlockGroup, error) {

	err := fs.OpenSelf()
	if err != nil {
		return nil, err
	}

	if n < 0 || n >= fs.Groups {
		return nil, fmt.Errorf("BlockGroup: no such block group as <%d>", n)
	}

	group := &BlockGroup{fs: fs, Num: n}

	group.Location = fs.BlocksPerGroup * n
	group.HasMetadata = fs.HasMetadataInBlockGroup(n)

	var m int64 // number of block group to take a block group descriptor from
	var desc *BlockGroupDesc

	for fails := 0; (fails < MaxFails) && (m < fs.Groups); fails++ {

		var metagroup *BlockGroup

		if n == 0 {
			metagroup = group
		} else {
			metagroup, err = fs.BlockGroup(m)
			if err != nil {
				break
			}
		}

		desc, err = metagroup.BlockGroupDesc(n)
		if err != nil {
			break
		}

		err = group.verify(desc)
		if err == nil {
			break
		}

		m++

		for !fs.HasMetadataInBlockGroup(m) {
			if m++; m >= fs.Groups {
				break
			}
		}
	}

	return group.initBy(desc), err
}

// initBy initializes group by properties of desc.
func (group *BlockGroup) initBy(desc *BlockGroupDesc) *BlockGroup {

	if desc == nil {
		return nil
	}

	ds := group.fs.GroupDescSize

	group.RawDesc = desc

	// should collect all needed info from block group descriptor
	group.BlockBitmapLocation = int64(desc.BlockBitmapLocationLow)
	if unsafe.Offsetof(desc.BlockBitmapLocationHigh)+4 <= uintptr(ds) {
		group.BlockBitmapLocation |= int64(desc.BlockBitmapLocationHigh) << 32
	}
	group.BlockBitmapChecksum = uint32(desc.BlockBitmapChecksumLow)
	group.BlockBitmapCmask = 0xffff
	if unsafe.Offsetof(desc.BlockBitmapChecksumHigh)+2 <= uintptr(ds) {
		group.BlockBitmapChecksum |= uint32(desc.BlockBitmapChecksumHigh) << 16
		group.BlockBitmapCmask |= 0xffff << 16
	}
	if group.BlockBitmapChecksum == 0 {
		group.BlockBitmapCmask = 0x0
	}
	group.BlockBitmapIsEmpty = desc.Flags&BG_BLOCK_UNINIT != 0

	group.InodeBitmapLocation = int64(desc.InodeBitmapLocationLow)
	if unsafe.Offsetof(desc.InodeBitmapLocationHigh)+4 <= uintptr(ds) {
		group.InodeBitmapLocation |= int64(desc.InodeBitmapLocationHigh) << 32
	}
	group.InodeBitmapChecksum = uint32(desc.InodeBitmapChecksumLow)
	group.InodeBitmapCmask = 0xffff
	if unsafe.Offsetof(desc.InodeBitmapLocationHigh)+2 <= uintptr(ds) {
		group.InodeBitmapChecksum |= uint32(desc.InodeBitmapChecksumHigh) << 16
		group.InodeBitmapCmask |= 0xffff << 16
	}
	if group.InodeBitmapChecksum == 0 {
		group.InodeBitmapCmask = 0x0
	}
	group.InodeBitmapIsEmpty = desc.Flags&BG_INODE_UNINIT != 0

	group.InodeTableLocation = int64(desc.InodeTableLocationLow)
	if unsafe.Offsetof(desc.InodeTableLocationHigh)+4 <= uintptr(ds) {
		group.InodeTableLocation |= int64(desc.InodeTableLocationHigh) << 32
	}
	group.InodeTableIsEmpty = desc.Flags&BG_INODE_ZEROED != 0

	return group
}

// verify veririfies desc checksum, if fs has meta_csum or gdt_csum feature enabled.
func (group *BlockGroup) verify(desc *BlockGroupDesc) error {

	if !group.fs.HasChecksums && !group.fs.HasGroupDescChecksums {
		return nil
	}

	// changing checksum value to 0x0 to calculate checksum of the hole structure
	csum := desc.Checksum
	desc.Checksum = 0

	prefix := new(bytes.Buffer)

	var calc uint16

	// here is 2 different ways, how to calculate checskum of block group descriptor
	switch {

	// if metadata_csum is set, then
	// crc32 of: super block seed + block group number + block group descriptor (with csum equal to 0)
	// initial value of checksum is checksum seed from super block
	case group.fs.HasChecksums:

		binary.Write(prefix, binary.LittleEndian, uint32(group.Num))

		var calc32 uint32

		calc32 = UpdateCrc32c(group.fs.ChecksumSeed, prefix.Bytes())
		calc32 = FinalizeCrc32c(calc32, desc.Bytes()[:group.fs.GroupDescSize])

		calc = uint16(calc32)

	// if metadata_csum is not set, but gdt_csum is set, then
	// crc16 of: UUID + block group number + block group descriptor (with csum skipped)
	case group.fs.HasGroupDescChecksums:

		// this variant not tested yet...

		binary.Write(prefix, binary.LittleEndian, group.fs.UUID.Bytes())
		binary.Write(prefix, binary.LittleEndian, uint32(group.Num))

		off, n := unsafe.Offsetof(desc.Checksum), unsafe.Sizeof(desc.Checksum)
		data := desc.Bytes()[:group.fs.GroupDescSize]

		calc = InitCrc16(prefix.Bytes())
		calc = FinalizeCrc16(calc, append(data[:off], data[off+n:]...))
	}

	desc.Checksum = csum
	return VerifyFormatOf(BlockGroupDescType, "checksum", desc.Checksum, calc)
}

// rangesOf returns ranges of free/used inodes/blocks by reading the corresponding bitmap.
func (group *BlockGroup) rangesOf(free bool, inodes bool) (ranges []Range, err error) {

	var n int64
	var data []byte

	bs := group.fs.BlockSize

	if inodes {
		n = group.fs.InodesPerGroup / 8
		data, err = group.fs.ReadBlocksAt(group.InodeBitmapLocation, (n+bs-1)/bs)
	} else {
		n = group.fs.BlocksPerGroup / 8
		data, err = group.fs.ReadBlocksAt(group.BlockBitmapLocation, (n+bs-1)/bs)
	}
	if err != nil {
		return nil, err
	}

	bitmap := Bitmap(data[:n])
	if free {
		ranges = bitmap.FreeRanges()
	} else {
		ranges = bitmap.UsedRanges()
	}

	if group.fs.HasChecksums {
		calc := FinalizeCrc32c(group.fs.ChecksumSeed, bitmap)
		if inodes {
			err = VerifyFormatOf(InodeBitmapType, "checksum", group.InodeBitmapChecksum, (calc & group.InodeBitmapCmask))
		} else {
			err = VerifyFormatOf(BlockBitmapType, "checksum", group.BlockBitmapChecksum, (calc & group.BlockBitmapCmask))
		}
	}

	return ranges, err
}

// FreeInodesRanges reads an inode bitmap and returns free inode ranges.
// Off and N fields of ranges are expressed in inodes, starting from first inode in group.
func (group *BlockGroup) FreeInodesRanges() ([]Range, error) {
	free, inodes := true, true
	return group.rangesOf(free, inodes)
}

// UsedInodesRanges reads an inode bitmap and returns used inode ranges.
// Off and N fields of ranges are expressed in inodes, starting from the first inode in group.
func (group *BlockGroup) UsedInodesRanges() ([]Range, error) {
	free, inodes := false, true
	return group.rangesOf(free, inodes)
}

// FreeBlocksRanges reads a block bitmap and returns free block ranges.
// Off and N fields of ranges are expressed in blocks, starting from the first block of group.
func (group *BlockGroup) FreeBlocksRanges() ([]Range, error) {
	free, inodes := true, false
	return group.rangesOf(free, inodes)
}

// UsedBlocksRanges reads a block bitmap and returns used block ranges.
// Off and N fields of ranges are expressed in blocks, starting from the first block of group.
func (group *BlockGroup) UsedBlocksRanges() ([]Range, error) {
	free, inodes := false, false
	return group.rangesOf(free, inodes)
}

// SuperBlock reads and returns super block, if it's present in group.
func (group *BlockGroup) SuperBlock() (*SuperBlock, error) {

	if !group.HasMetadata {
		return nil, fmt.Errorf("SuperBlock: no super block in block group <%d>", group.Num)
	}

	sb := new(SuperBlock)

	// offset to the block group
	off := group.fs.BlockSize * group.Location
	err := sb.ReadFrom(io.NewSectionReader(group.fs, off, SuperBlockSize))
	if err != nil {
		return nil, err
	}

	// not verifying right checksum value
	return sb, nil
}

// BlockGroupDesc reads and returns descriptor of n block group, if it's present in group.
func (group *BlockGroup) BlockGroupDesc(n int64) (*BlockGroupDesc, error) {

	var groups int64 // number of groups in the table of group descriptor, if it's present
	if group.fs.HasMetaGroups {
		groups = group.fs.GroupsPerMetaGroup
	} else {
		groups = group.fs.Groups
	}

	if !group.HasMetadata || (n < 0) || (n >= groups) {
		return nil, fmt.Errorf("BlockGroupDesc: no descriptor of block group <%d> in block group <%d>", n, group.Num)
	}

	desc := new(BlockGroupDesc)

	// offset to the block group + super block size + offset to the block group descriptor in the table
	off := (group.fs.BlockSize * group.Location) + group.fs.BlockSize + (group.fs.GroupDescSize * n)
	err := desc.ReadFrom(io.NewSectionReader(group.fs, off, BlockGroupDescSize))
	if err != nil {
		return nil, err
	}

	// not verifying right checksum value
	return desc, nil
}
