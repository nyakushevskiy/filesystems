package ext4

import (
	"fmt"
)

// Range reprsents a range [off, off+n).
type Range struct {
	Off, N int64
}

// String returns a textual representation of range.
func (r Range) String() string {
	switch {
	case r.N == 1:
		return fmt.Sprintf("%v", r.Off)
	case r.N > 1:
		return fmt.Sprintf("%v-%v", r.Off, r.Off+r.N-1)
	default:
		return ""
	}
}

// Bitmap represents used bitmap of X,
// where every set bit means that X is used, otherwise that it's free.
type Bitmap []byte

// ranges returns ranges of m that satisfies inRange condition.
func (m Bitmap) ranges(inRange func(int) bool) (ranges []Range) {

	var r Range

	for bit := 0; bit < len(m)*8; bit++ {

		if inRange(bit) {
			r.N++
		} else {
			if r.N != 0 {
				ranges = append(ranges, r)
			}
			r.Off, r.N = int64(bit+1), 0
		}

	}

	if r.N != 0 {
		ranges = append(ranges, r)
	}

	return ranges
}

// FreeRanges returns free ranges of m.
func (m Bitmap) FreeRanges() []Range {
	return m.ranges(m.IsFree)
}

// UsedRanges returns used ranges of m.
func (m Bitmap) UsedRanges() []Range {
	return m.ranges(m.IsUsed)
}

// IsFree reports whether n unit of bitmap is free.
func (m Bitmap) IsFree(n int) bool {
	nByte, nBit := uint(n/8), uint(n%8)
	return (m[nByte] & (1 << nBit)) == 0
}

// IsUsed reports whether n uint of bitmap is used.
func (m Bitmap) IsUsed(n int) bool {
	nByte, nBit := uint(n/8), uint(n%8)
	return (m[nByte] & (1 << nBit)) != 0
}
