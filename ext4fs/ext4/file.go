package ext4

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

// File represents a file.
type File struct {
	fs    *Ext4
	inode *Inode
	name  string
}

// OpenRoot opens a root of fs.
func (fs *Ext4) OpenRoot() (*File, error) {

	if fs.root != nil {
		return fs.root, nil
	}

	err := fs.OpenSelf()
	if err != nil {
		return nil, err
	}

	inode, err := fs.Inode(2) // number of root inode is 2
	if err != nil {
		return nil, err
	}

	fs.root = &File{fs, inode, string(filepath.Separator)}

	return fs.root, nil
}

// OpenRoot opens a file by name - absolute path to file on fs.
func (fs *Ext4) OpenFile(name string) (*File, error) {

	err := fs.OpenSelf()
	if err != nil {
		return nil, err
	}

	if !filepath.IsAbs(name) {
		return nil, fmt.Errorf("OpenFile: %q is not absolute path", name)
	}

	root, err := fs.OpenRoot()
	if err != nil {
		return nil, err
	}

	if root.name == name {
		return root, nil
	}

	var (
		file *File
		dir  *File = root
	)

	for _, name := range SplitPath(name) {
		file, err = dir.FindFile(name)
		if err != nil {
			return nil, err
		}
		dir = file
	}

	return file, err
}

// Name returns path related to the root of the file system.
func (file *File) Name() string {
	return file.name
}

// Info describes a file and is returned by Stat() method of VirtFile.
type FileInfo struct {
	Name    string
	Size    int64
	ModTime time.Time
	Mode    os.FileMode
}

// Stat returns FileInfo structure describing this file.
func (file *File) Stat() (*FileInfo, error) {
	info := &FileInfo{
		Name:    file.name,
		Size:    file.inode.DataSize,
		Mode:    file.inode.Mode,
		ModTime: file.inode.ModTime,
	}
	return info, nil
}

// Inode returns the inode, associated with this file.
func (file *File) Inode() *Inode {
	return file.inode
}

// ForEachDataBlock calls f function
// with data and locations of every block of file.
func (file *File) ForEachDataBlock(f func(data []byte, location int64) bool) error {
	return ForEachBlock(file.inode.DataBlockIter(), f)
}

// ForEachBlock calls f function
// with nil as data and locations of every block of file.
func (file *File) ForEachBlock(f func(data []byte, location int64) bool) error {
	return ForEachBlock(file.inode.DataBlockIter(), f)
}

// ReadAll reads all file data.
func (file *File) ReadAll() (data []byte, err error) {

	buf := new(bytes.Buffer)

	if file.inode.HasInlineData {
		_, err = buf.Write(file.inode.InlineData)
	}

	var internalErr error

	err = file.ForEachDataBlock(func(data []byte, _ int64) bool {
		_, internalErr = buf.Write(data)
		return internalErr == nil
	})
	if err != nil {
		return nil, err
	}
	if internalErr != nil {
		return nil, internalErr
	}

	size := int(file.inode.DataSize)
	if 0 < size && size < buf.Len() {
		buf.Truncate(size)
	}

	return buf.Bytes(), nil
}

// IsRegular reports whether this file is regular.
func (file *File) IsRegular() bool {
	return file.inode.Mode.IsRegular()
}

// IsDir reports whether this file is directory.
func (file *File) IsDir() bool {
	return file.inode.Mode.IsDir()
}

// file returns file with inodeNum and fileName in dir.
func (dir *File) file(inodeNum int64, fileName string) (*File, error) {

	var err error
	var inode *Inode

	if dir.inode.group.Num == (inodeNum-1)/dir.fs.InodesPerGroup {
		inode, err = dir.inode.group.Inode((inodeNum - 1) % dir.fs.InodesPerGroup)
	} else {
		inode, err = dir.fs.Inode(inodeNum)
	}
	if err != nil {
		return nil, err
	}

	return &File{dir.fs, inode, filepath.Join(dir.name, fileName)}, nil
}

// ForEachDirEntry calls f function
// with inode number and file name of every dir entry of dir, if IsDir() returns true.
func (dir *File) ForEachDirEntry(f func(inodeNum int64, name string) bool) error {

	if !dir.IsDir() {
		return fmt.Errorf("ForEachDirEntry: %q is not a directory", dir.name)
	}

	var entry DirEntry

	if dir.inode.HasInlineData {
		// first 4 bytes is the inode number of the parent directory,
		// which will be anyway skipped during listing files of the directory, so just skipping it here
		rd := bytes.NewReader(dir.inode.InlineData[4:])
		for {
			err := entry.ReadFrom(rd)
			if err == io.EOF {
				break
			}
			if err != nil {
				return err
			}
			if entry.InodeNum == 0 {
				break
			}
			if !f(int64(entry.InodeNum), string(entry.Name)) {
				break
			}
		}
	}

	var internalErr error

	err := dir.ForEachDataBlock(func(block []byte, _ int64) bool {

		rd := bytes.NewReader(block)

		for {
			var entry DirEntry

			internalErr = entry.ReadFrom(rd)
			if internalErr == io.EOF {
				internalErr = nil
				return true
			}
			if internalErr != nil {
				return false
			}
			if entry.InodeNum == 0 {
				return true
			}

			if !f(int64(entry.InodeNum), string(entry.Name)) {
				return false
			}
		}

		return true
	})

	if err != nil {
		return err
	}

	return internalErr
}

// ListFiles lists file of dir, if IsDir() returns true.
func (dir *File) ListFiles() ([]*File, error) {

	var files []*File

	err := dir.ForEachDirEntry(func(inodeNum int64, fileName string) bool {

		if fileName == "." || fileName == ".." {
			return true
		}

		file, internalErr := dir.file(inodeNum, fileName)
		if internalErr == nil {
			files = append(files, file)
		}

		return true
	})

	return files, err
}

// FindFile tries to find file with name in dir, if IsDir() returns true.
func (dir *File) FindFile(name string) (*File, error) {

	var file *File
	var internalErr error

	err := dir.ForEachDirEntry(func(inodeNum int64, fileName string) bool {

		if name != fileName {
			return true
		}

		file, internalErr = dir.file(inodeNum, fileName)
		return false
	})

	if err != nil {
		return nil, err
	}
	if internalErr != nil {
		return nil, internalErr
	}
	if file == nil {
		return nil, NotFound(dir.Name(), name)
	}

	return file, nil
}
