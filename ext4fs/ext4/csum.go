package ext4

import (
	"hash/crc32"

	"github.com/howeyc/crc16"
)

// Tables of crc32c and crc16 varations of algorithms used in EXT4 fs.
var (
	Crc32cTab = crc32.MakeTable(crc32.Castagnoli)
	Crc16Tab  = crc16.MakeTableNoXOR(crc16.IBM)
)

// To calculate X (Crc32c or Crcr16) checksum
// functions should be called in order: InitX -> (UpdateX ->)... -> FinalizeX,
// also it could be done in one set by X.

// InitCrc32c returns new crc32c checksum initialiazed with data.
func InitCrc32c(data []byte) uint32 {
	return crc32.Update(0, Crc32cTab, data)
}

// UpdateCrc32c returns crc32c checksum of csum updated with data.
func UpdateCrc32c(csum uint32, data []byte) uint32 {
	return crc32.Update(csum, Crc32cTab, data)
}

// FinalizeCrc32c returns crc32 checksum of csum finalized with data.
func FinalizeCrc32c(csum uint32, data []byte) uint32 {
	return ^crc32.Update(csum, Crc32cTab, data)
}

// Crc32c returns crc32c checksum of data.
func Crc32c(data []byte) uint32 {
	return ^crc32.Update(0, Crc32cTab, data)
}

// InitCrc16 returns new crc16 checksum initialiazed with data.
func InitCrc16(data []byte) uint16 {
	return crc16.Update(0xffff, Crc16Tab, data)
}

// UpdateCrc16 returns crc16 checksum of csum updated with data.
func UpdateCrc16(csum uint16, data []byte) uint16 {
	return crc16.Update(csum, Crc16Tab, data)
}

// FinalizeCrc16 returns crc16 checksum of csum finalized with data.
func FinalizeCrc16(csum uint16, data []byte) uint16 {
	return crc16.Update(csum, Crc16Tab, data)
}

// Crc16 returns crc16 checksum of data.
func Crc16(data []byte) uint16 {
	return crc16.Update(0xffff, Crc16Tab, data)
}
