package ext4

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
	"unsafe"

	"github.com/satori/go.uuid"
)

const Type = "EXT4"

// State represents a state, that a file system could be in.
type State int16

const (
	CleanlyUnmounted = State(1 << iota)
	ErrorsDetected
	OrphansRecovered
)

// String returns plain text name of state.
func (state State) String() string {
	switch state {
	case CleanlyUnmounted:
		return "cleanly unmounted"
	case ErrorsDetected:
		return "errors detected"
	case OrphansRecovered:
		return "orphans being recovered"
	default:
		return "unknown file system state"
	}
}

// ErrorHandling represents behaviour of a file system on error.
type ErrorHandling int16

const (
	ContinueOnError = ErrorHandling(1 + iota)
	RemountReadOnlyOnError
	PanicOnError
)

// String returns plain text name of errors.
func (errors ErrorHandling) String() string {
	switch errors {
	case ContinueOnError:
		return "continue on error"
	case RemountReadOnlyOnError:
		return "remount read only on error"
	case PanicOnError:
		return "panic on error"
	default:
		return "unknown file system error handling"
	}
}

// Os represents os, which a file system was created by.
type OS int32

const (
	Linux = OS(iota)
	Hurd
	Masix
	FreeBSD
	Lites
)

// String returns plain text name of os.
func (os OS) String() string {
	switch os {
	case Linux:
		return "Linux"
	case Hurd:
		return "Hurd"
	case Masix:
		return "Masix"
	case FreeBSD:
		return "FreeBSD"
	case Lites:
		return "Lites"
	default:
		return "unknown creator os"
	}
}

// CompatFeatures reprsents a file system compatible feature set.
// A file system could be mounted,
// even if the driver doesn't know any active feature from this set.
type CompatFeatures uint32

const (
	COMPAT_DIR_PREALLOC = CompatFeatures(1 << iota)
	COMPAT_IMAGIC_NODES
	COMPAT_HAS_JOURNAL
	COMPAT_EXT_ATTR
	COMPAT_RESIZE_INODE
	COMPAT_DIR_INDEX
	COMPAT_LAZY_BG
	COMPAT_EXCLUDE_INODE
	COMPAT_EXCLUDE_BITMAP
)

var CompatFeatureNames = [...]string{
	"dir_prealloc",
	"imagic_nodes",
	"has_journal",
	"ext_attr",
	"resize_inode",
	"dir_index",
	"lazy_bg",
	"exclude_inode",
	"exclude_bitmap",
}

// Names returns names of active features in features.
func (features CompatFeatures) Names() []string {
	return FlagNames(uint64(features), CompatFeatureNames[:])
}

// IncompatFeatures reprsents a file system incompatible feature set.
// A file system couldn't be mounted,
// if the driver doesn't know some active feature from this set.
type IncompatFeatures uint32

const (
	INCOMPAT_COMPRESSION = IncompatFeatures(1 << iota)
	INCOMPAT_FILETYPE
	INCOMPAT_RECOVER
	INCOMPAT_JOURNAL_DEV
	INCOMPAT_META_BG
	_
	INCOMPAT_EXTENT
	INCOMPAT_64BIT
	INCOMPAT_MMP
	INCOMPAT_FLEX_BG
	INCOMPAT_EA_INODE
	_
	INCOMPAT_DIRDATA
	INCOMPAT_CSUM_SEED
	INCOMPAT_LARGEDIR
	INCOMPAT_INLINE_DATA
	INCOMPAT_ENCRYPT
)

var IncompatFeatureNames = [...]string{
	"compression",
	"filetype",
	"recover",
	"journal_dev",
	"meta_bg",
	"",
	"extent",
	"64bit",
	"mmp",
	"flex_bg",
	"ea_inode",
	"",
	"dirdata",
	"csum_seed",
	"large_dir",
	"inline_data",
	"encrypt",
}

// Names returns names of active features in features.
func (features IncompatFeatures) Names() []string {
	return FlagNames(uint64(features), IncompatFeatureNames[:])
}

// RoCompatFeatures reprsents a file system read-only compatible feature set.
// A file system could be mounted, but only with read-only access,
// if the driver doesn't know some active feature from this set.
type RoCompatFeatures uint32

const (
	RO_COMPAT_SPARSE_SUPER = RoCompatFeatures(1 << iota)
	RO_COMPAT_LARGE_FILE
	RO_COMPAT_BTREE_DIR
	RO_COMPAT_HUGE_FILE
	RO_COMPAT_GDT_CSUM
	RO_COMPAT_DIR_NLINK
	RO_COMPAT_EXTRA_ISIZE
	RO_COMPAT_HAS_SNAPSHOT
	RO_COMPAT_QUOTA
	RO_COMPAT_BIGALLOC
	RO_COMPAT_METADATA_CSUM
	RO_COMPAT_REPLICA
	RO_COMPAT_READONLY
	RO_COMPAT_PROJECT
)

var RoCompatFeatureNames = [...]string{
	"sparse_super",
	"large_file",
	"btree_dir",
	"huge_file",
	"uninit_bg", // gdt_csum, actually, but somehow this is the name of this feature
	"dir_nlink",
	"extra_isize",
	"has_snapshot",
	"quota",
	"bigalloc",
	"metadata_csum",
	"replica",
	"readonly",
	"project",
}

// Names returns names of active features in features.
func (features RoCompatFeatures) Names() []string {
	return FlagNames(uint64(features), RoCompatFeatureNames[:])
}

// Time32 is a 32-bit a time in seconds since epoch.
type Time32 int32

// Time converts t to time.Time.
func (t Time32) Time() time.Time {
	return time.Unix(int64(t), 0)
}

// String returns formatted time string.
func (t Time32) String() string {
	return t.Time().String()
}

// Time64 is a 64-bit a time in seconds since epoch with extra precision.
type Time64 int64

// Time converts t to time.Time.
func (t Time64) Time() time.Time {
	sec, nsec := int32(t), int32(t>>32)
	return time.Unix(int64(sec), int64(nsec))
}

// String returns a textual representation of t.
func (t Time64) String() string {
	return t.Time().String()
}

// SetExtraPrecision sets extra precision of t to extra.
func (t *Time64) SetExtraPrecision(extra Time32) {
	*t = Time64(extra)<<32 | Time64(Time32(*t))
}

// Duration16 is a 16-bit duration in seconds.
type Duration16 int16

// Duration converts d to time.Duration.
func (d Duration16) Duration() time.Duration {
	return time.Duration(d)
}

// String returns textual representation of d.
func (d Duration16) String() string {
	return fmt.Sprintf("%d seconds", d.Duration())
}

// Duration32 is a 32-bit duration in seconds.
type Duration32 int32

// Duration converts d to time.Duration.
func (d Duration32) Duration() time.Duration {
	return time.Duration(d)
}

// String returns textual representation of d.
func (d Duration32) String() string {
	return fmt.Sprintf("%d seconds", d.Duration())
}

// HashAlgorithm represents hash algorithm used in hash tree directories.
type HashAlgorithm int8

const (
	LegacyHash = HashAlgorithm(iota)
	HalfMD5Hash
	TeaHash
	UnsignedLegacyHash
	UnsignedHalfMD5Hash
	UnsignedTeaHash
)

// String returns a plain text name of algo.
func (algo HashAlgorithm) String() string {
	switch algo {
	case LegacyHash:
		return "Legacy"
	case HalfMD5Hash:
		return "Half MD4"
	case TeaHash:
		return "Tea"
	case UnsignedLegacyHash:
		return "Legacy, unsigned"
	case UnsignedHalfMD5Hash:
		return "Half MD4, unsigned"
	case UnsignedTeaHash:
		return "Tea, unsigned"
	default:
		return "unkown hash version"
	}
}

// CryptoAlogrithm represents crypto algorithm used by a file system.
type CryptoAlgorithm int8

const (
	InvalidCrypto = CryptoAlgorithm(iota)
	XTSCrypto
	GCMCrypto
	CBCCrypto
)

// String returns a plain text name of algo.
func (algo CryptoAlgorithm) String() string {
	switch algo {
	case InvalidCrypto:
		return "invalid crypto algorithm"
	case XTSCrypto:
		return "256-bit AES in XTS mode"
	case GCMCrypto:
		return "256-bit AES in GCM mode"
	case CBCCrypto:
		return "256-bit AES in CBC mode"
	default:
		return "unknown crypto algorithm"
	}
}

// ChecksumAlgorithm represents a checksum alogrithm used by a file system.
type ChecksumAlgorithm int8

// The desired checksum algorithm is indicated in the superblock,
// though as of June 2018 the only supported algorithm is crc32c
const (
	NoChecksum = ChecksumAlgorithm(iota)
	Crc32Checksum
)

// String returns a plain text name of algo.
func (algo ChecksumAlgorithm) String() string {
	switch algo {
	case NoChecksum:
		return "no checksum"
	case Crc32Checksum:
		return "crc32c/crc16"
	default:
		return "unsupported checksum algorithm"
	}
}

const (
	SuperBlockSize      = 1024
	SuperBlockSignature = uint16(0xEF53)
	SuperBlockType      = Type + " super block"
)

// SuperBlock is a raw on disk super block structure.
type SuperBlock struct {

	// Total number of inodes and blocks
	TotalInodes    int32
	TotalBlocksLow int32

	// This number of blocks can only be allocated by the super-user
	ReservedBlocksLow int32

	// Number of free inodes and blocks
	FreeBlocksLow int32
	FreeInodes    int32

	// First data block.
	// This must be at least 1 for 1K-block filesystems
	// and is typically 0 for all other block sizes
	FirstDataBlockIdx int32

	// Block size is 2 to the power of (10 + LogExtraBlockSize) bytes
	LogExtraBlockSize uint32
	// Block size is 2 to the power of LogBlockSize blocks
	// if bigalloc is enabled.
	// Otherwise LogBlockSize must equal to LogExtraBlockSize
	LogBlockSize uint32

	// Blocks per block group
	BlocksPerGroup int32
	// Blocks per block group,
	// if bigalloc is enabled.
	// Otherwise BlocksPerGroup must equal to BlocksPerGroup
	ClustersPerGroup int32
	// Inodes per block group
	InodesPerGroup int32

	MountTime Time32
	WriteTime Time32

	// Number of mounts since the last fsck
	Mounts int16
	// Number of mounts beyond which a fsck is needed
	MaxMounts int16

	// Super block signature, is always 0xEF53
	Signature uint16

	// File system state. Valid values are:
	// 0x0001 	Cleanly umounted
	// 0x0002 	Errors detected
	// 0x0004 	Orphans being recovered
	State State

	// Behaviour when detecting errors. One of:
	// 1 	Continue
	// 2 	Remount read-only
	// 3 	Panic
	ErrorHandling ErrorHandling

	MinorRevisionLevel int16

	LastCheckTime Time32
	CheckInterval Duration32

	// OS. One of:
	// 0 	Linux
	// 1 	Hurd
	// 2 	Masix
	// 3 	FreeBSD
	// 4 	Lites
	OS OS

	// Revision level. One of:
	// 0	Original format
	// 1	v2 format w/ dynamic inode sizes
	RevisionLevel int32

	// Default uid, gid for reserved blocks
	DefaultUID int16
	DefaultGID int16

	// First non-reserved inode
	FirstFreeInode int32

	// Size of inode structure, in bytes
	InodeSize int16

	// Block group number of this superblock
	GroupIdx uint16

	// Compatible features flag set. Kernel can still
	// read/write this fs even if it doesn't understand a
	// flag; fsck should not do that
	CompatFeatures CompatFeatures
	// Incompatible features flag set. If the kernel or fsck
	// doesn't understand one of these bits, it should stop
	IncompatFeatures IncompatFeatures
	// Readonly-compatible feature flag set. If the kernel
	// doesn't understand one of these bits, it can still
	// mount read-only
	RoCompatFeatures RoCompatFeatures

	VolumeUUID uuid.UUID
	VolumeName [16]byte

	// Directory where filesystem was last mounted
	MountedAt [64]byte

	// For compression (can't find info...)
	CompressionFlags int32

	// Number of locks to try to preallocate for ... files?
	PreallocBlocks int8
	// Number of blocks to preallocate for directories
	PreallocDirBlocks int8

	// Number of reserved GDT entries
	// for future filesystem expansion
	ReservedGDTBlocks int16

	// UUID of journal superblock
	JournalUUID uuid.UUID
	// inode number of journal file
	JournalInode int32
	// Device number of journal file,
	// if the external journal feature flag is set
	JournalDevice int32

	// Start of list of orphaned inodes to delete
	LastOrphanedInode int32

	// HTREE hash seed
	HashSeed uuid.UUID

	// Default hash algorithm to use for directory hashes. One of:
	// 0x0	Legacy.
	// 0x1	Half MD4.
	// 0x2	Tea.
	// 0x3	Legacy, unsigned.
	// 0x4	Half MD4, unsigned.
	// 0x5	Tea, unsigned.
	HashAlgorithm HashAlgorithm

	// If this value is 0 or EXT3_JNL_BACKUP_BLOCKS,
	// then the JournalBackupCopy field
	// contains a duplicate copy of the inodes array
	JournalBackupType int8

	// Size of group descriptors, in bytes,
	// if the 64bit incompat feature flag is set.
	WantBlockGroupDescSize int16

	// Default mount options
	DefaultMountOptionFlags int32

	// First metablock block group,
	// if this feature is enabled.
	FirstMetaGroupIdx int32

	// When the filesystem was created
	CreateTime Time32

	// See JournalBackupType
	JournalBackupCopy [17]int32

	// High bits of TotalBlocks
	TotalBlocksHigh int32
	// High bits of ReservedBlocks
	ReservedBlocksHigh int32
	// High bits of FreeBlocks
	FreeBlocksHigh int32

	// All inodes have at least NeedExtraInodeSize bytes
	NeedExtraInodeSize int16
	// New inodes should reserve WantExtraInodeSize bytes
	WantExtraInodeSize int16

	// Miscellaneous flags. Any of:
	// 0x0001	Signed directory hash in use.
	// 0x0002	Unsigned directory hash in use.
	// 0x0004	To test development code.
	Flags int32

	// This is the number of logical blocks read from
	// or written to the disk before moving to the next disk.
	// This affects the placement of filesystem metadata,
	// which will hopefully make RAID storage faster.
	RAIDStride int16

	// Seconds to wait in multi-mount prevention (MMP) checking.
	// In theory, MMP is a mechanism to record in the superblock
	// which host and device have mounted the filesystem,
	// in order to prevent multiple mounts.
	// This feature does not seem to be implemented...
	MMPInterval Duration16
	// Block for multi-mount protection data.
	MMPBlockIdx int64

	// This is the number of logical blocks read from or written to the disk
	// before coming back to the current disk.
	// This is used by the block allocator to try
	// to reduce the number of RMW operations in a RAID5/6.
	RAIDStripeWidth int32

	// Size of a flexible block group is 2 to the power of LogGroupsPerFlex.
	LogGroupsPerFlex uint8

	// Metadata checksum algorithm type. The only valid value is 1 (crc32c)
	ChecksumAlgorithm ChecksumAlgorithm

	// Reserved bytes to rich 1024 bytes structure size
	Reserved1 [2]byte

	// Number of KiB written to this filesystem over its lifetime
	WrittenKiB int64

	// Inode number, sequence ID
	// and reserved blocks count for future use for active snapshot
	ActiveSnapshotInodeNum       int32
	ActiveSnapshotID             int32
	ActiveSnapshotReservedBlocks int64

	// Inode number of the head of the on-disk snapshot list
	SnapshotListHeadInodeNum int32

	// Total number of errors happened
	Errors int32

	// Description of first happened error
	FirstErrorTime     Time32
	FirstErrorInodeNum int32
	FirstErrorBlockIdx int64
	FirstErrorFuncName [32]byte
	FirstErrorLine     int32

	// Description of last happened error
	LastErrorTime     Time32
	LastErrorInodeNum int32
	LastErrorLine     int32
	LastErrorBlockIdx int64
	LastErrorFuncName [32]byte

	// ASCIIZ string of mount options
	MountOptionsString [64]byte

	// Inode number of user and group quota file
	// not an index, actually, because starts from 1
	UserQuotaInodeNum  int32
	GroupQuotaInodeNum int32

	// Overhead blocks/clusters in fs
	OverheadBlocks int32

	// Block groups containing superblock backups
	BackupGroups [2]uint32

	// Encryption algorithms in use.
	// There can be up to four algorithms in use at any time.
	CryptoAlgorithms [4]CryptoAlgorithm
	// Salt for the string2key algorithm for encryption.
	CryptoSalt uuid.UUID

	// Number of inode lost+found
	// not an index, actually, because starts from 1
	LostAndFoundInodeNum int32

	// Number of inode that tracks project quotas
	// not an index, actually, because starts from 1
	TrackQuotasInodeNum int32

	// Checksum seed used for metadata checksum calculations.
	WantChecksumSeed uint32

	// Reserved bytes to rich 1024 bytes structure size
	Reserved2 [392]byte

	// SuperBlock checksum
	Checksum uint32
}

// ReadFrom reads sb from r.
func (sb *SuperBlock) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, sb)
	if err != nil {
		return err
	}

	return VerifyFormatOf(SuperBlockType, "signature", sb.Signature, SuperBlockSignature)
}

// WriteTo writes sb to w.
func (sb *SuperBlock) WriteTo(w io.Writer) error {
	return binary.Write(w, binary.LittleEndian, sb)
}

// Bytes returns sb in binary form.
func (sb *SuperBlock) Bytes() []byte {

	buf := new(bytes.Buffer)
	sb.WriteTo(buf)

	return buf.Bytes()
}

// Info implements Info() method of fs.VirtFileSystem.
func (sb *SuperBlock) Info() *Info {

	info := new(Info)

	info.Type = Type
	info.ID = sb.VolumeUUID.String()

	mountedAt := string(sb.MountedAt[:])
	if last := strings.Index(mountedAt, "\x00"); last > 0 {
		mountedAt = mountedAt[:last]
	}
	info.MountedAt = mountedAt

	createdAt := sb.CreateTime
	info.CreateTime = createdAt.Time()

	info.Features = append(info.Features, sb.CompatFeatures.Names()...)
	info.Features = append(info.Features, sb.IncompatFeatures.Names()...)
	info.Features = append(info.Features, sb.RoCompatFeatures.Names()...)

	info.TotalBlocks = int64(sb.TotalBlocksHigh)<<32 | int64(sb.TotalBlocksLow)
	info.FreeBlocks = int64(sb.FreeBlocksHigh)<<32 | int64(sb.FreeBlocksHigh)

	info.BlockSize = int(1 << (10 + sb.LogExtraBlockSize))

	return info
}

// BlockGroupFlags is a set of flags in a block group descriptor.
type BlockGroupFlags uint16

const (
	BG_INODE_UNINIT = BlockGroupFlags(1 << iota)
	BG_BLOCK_UNINIT
	BG_INODE_ZEROED
)

// String returns plain text name of flags.
var BlockGroupFlagNames = [...]string{
	"inode_uninit",
	"block_uninit",
	"inode_zeroed",
}

// Names returns names of active features in features.
func (flags BlockGroupFlags) Names() []string {
	return FlagNames(uint64(flags), BlockGroupFlagNames[:])
}

const (
	BlockGroupDescSize = 64
	BlockGroupDescType = Type + " block group descriptor"

	BlockBitmapType = Type + " block bitmap"
	InodeBitmapType = Type + " inode bitmap"
)

// BlockGroupDesc is a raw block group descriptor on disk structure.
type BlockGroupDesc struct {

	// Lower bits of locations
	BlockBitmapLocationLow uint32
	InodeBitmapLocationLow uint32
	InodeTableLocationLow  uint32

	// Lower bits of counts
	FreeBlocksLow      uint16
	FreeInodesLow      uint16
	UsedDirectoriesLow uint16

	// Block group features. Any of:
	// 	0x1 inode table and bitmap are not initialized
	// 	0x2 block bitmap is not initialized
	// 	0x4 inode table is zeroed
	Flags BlockGroupFlags

	// Lower bits of location
	SnapshotExcludeBitmapLocationLow uint32

	// Lower bits of checksums
	BlockBitmapChecksumLow uint16
	InodeBitmapChecksumLow uint16

	// Lower bits of unused inode count
	UnusedInodesLow uint16

	Checksum uint16

	// -------------------------------------------------------
	// The next fields only exist
	// if the 64-bit feature is enabled
	// and BlockGroupDescSize > 32
	// -------------------------------------------------------

	// High bits of locations
	BlockBitmapLocationHigh int32
	InodeBitmapLocationHigh int32
	InodeTableLocationHigh  int32

	// High bits of counts
	FreeBlocksHigh      int16
	FreeInodesHigh      int16
	UsedDirectoriesHigh int16
	UnusedInodesHigh    int16

	// High bits of location
	SnapshotExcludeBitmapLocationHigh int32

	// High bits of checksums
	BlockBitmapChecksumHigh uint16
	InodeBitmapChecksumHigh uint16

	// Reserved bytes to rich 64 bytes of the structure
	Reserved [4]byte
}

// ReadFrom reads desc from r.
func (desc *BlockGroupDesc) ReadFrom(r io.Reader) error {
	return binary.Read(r, binary.LittleEndian, desc)
}

// WriteTo writes desc to w.
func (desc *BlockGroupDesc) WriteTo(w io.Writer) error {
	return binary.Write(w, binary.LittleEndian, desc)
}

// Bytes returns desc in binary form.
func (desc *BlockGroupDesc) Bytes() []byte {

	buf := new(bytes.Buffer)
	desc.WriteTo(buf)

	return buf.Bytes()
}

// InodeFlags is a set of flags in an inode.
type InodeFlags uint32

const (
	SECRM_FL = InodeFlags(1 << iota)
	UNRM_FL
	COMPR_FL
	SYNC_FL
	IMMUTABLE_FL
	APPEND_FL
	NODUMP_FL
	NOATIME_FL
	DIRTY_FL
	COMPRBLK_FL
	NOCOMPR_FL
	ENCRYPT_FL
	INDEX_FL
	IMAGIC_FL
	JOURNAL_DATA_FL
	NOTAIL_FL
	DIRSYNC_FL
	TOPDIR_FL
	HUGE_FILE_FL
	EXTENTS_FL
	_
	EA_INODE_FL
	EOFBLOCKS_FL
	_
	SNAPFILE_FL
	_
	SNAPFILE_DELETED_FL
	SNAPFILE_SHRUNK_FL
	INLINE_DATA_FL
	PROJINHERIT_FL

	InodeUserVisibleFlags    = 0x4BDFFF
	InodeUserModifiableFlags = 0x4B80FF
)

var InodeFlagNames = [...]string{
	"secrm",
	"unrm",
	"compr",
	"sync",
	"immutable",
	"append",
	"nodump",
	"noatime",
	"dirty",
	"compblk",
	"encrypt",
	"index",
	"imagic",
	"journal_data",
	"notail",
	"dirsync",
	"topdir",
	"huge_file",
	"extents",
	"",
	"ea_inode",
	"eofblocks",
	"",
	"snapfile",
	"",
	"snapfile_deleted",
	"snapfile_shrunk",
	"inline_data",
	"projinherit",
}

// Names returns names of active features in features.
func (flags InodeFlags) Names() []string {
	return FlagNames(uint64(flags), InodeFlagNames[:])
}

// FileMode reprsents a file mode specified in an inode.
type FileMode uint16

const (
	S_IXOTH = FileMode(1 << iota) // others may execute
	S_IWOTH                       // others may write
	S_IROTH                       // others may read
	S_IXGPR                       // group may execute
	S_IWGRP                       // group may write
	S_IRGRP                       // group may read
	S_IXUSR                       // user may execute
	S_IWUSR                       // user may write
	S_IRUSR                       // user may read
	S_ISVTX                       // sticky bit
	S_ISGID                       // gid is set
	S_ISUID                       // uid is set

	FilePermMask = FileMode(0x01FF)
)

const (
	S_IFIFO  = FileMode(0x1000) // is fifo
	S_IFCHR  = FileMode(0x2000) // is character device
	S_IFDIR  = FileMode(0x4000) // is directory
	S_IFBLK  = FileMode(0x6000) // is block device
	S_IFREG  = FileMode(0x8000) // is regular file
	S_IFLNK  = FileMode(0xA000) // is symbolic link
	S_IFSOCK = FileMode(0xC000) // is socket

	FileTypeMask = FileMode(0xF000)
)

// Os converts mode to os.FileMode.
func (mode FileMode) Os() os.FileMode {

	var osmode os.FileMode

	switch mode & FileTypeMask {
	case S_IFIFO:
		osmode |= os.ModeNamedPipe
	case S_IFCHR:
		osmode |= os.ModeDevice | os.ModeCharDevice
	case S_IFDIR:
		osmode |= os.ModeDir
	case S_IFBLK:
		osmode |= os.ModeDevice
	case S_IFLNK:
		osmode |= os.ModeSymlink
	case S_IFSOCK:
		osmode |= os.ModeNamedPipe
	}

	osmode |= os.FileMode(mode & FilePermMask)

	return osmode
}

// String returns a textual representation of mode.
func (mode FileMode) String() string {
	return mode.Os().String()
}

const (
	InodeEntrySize = 156
	InodeEntryType = Type + " inode"
)

// InodeEntry represents a raw inode on disk structure.
type InodeEntry struct {

	// File mode
	Mode FileMode

	// Lower 16-bits of Owner UID
	OwnerUidLow uint16

	// Lower 32-bits of size in bytes
	SizeLow uint32

	// times in seconds since the epoch
	AccessTime   Time32
	ChangeTime   Time32
	ModTime      Time32
	DeletionTime Time32

	// Lower 16-bits of GID
	OwnerGidLow uint16

	HardLinks uint16

	// Lower 32-bits of 512-byte blocks count.
	TotalBlocksLow uint32

	// Inode flags
	Flags InodeFlags

	// Lower 32-bits of inode version
	VersionLow uint32

	// Block map or extent tree
	Iblock [60]byte

	// File version (for NFS)
	Generation uint32

	// Lower 32-bits of extended attribute block
	XattrLocationLow uint32

	// Upper 32-bits of file/directory size
	SizeHigh uint32

	// (Obsolete) fragment address
	FragmentAddress uint32

	// Upper 16-bits of the block count
	TotalBlocksHigh uint16

	// High 16-bits of extended attribute block
	XattrLocationHigh uint16

	// Upper 16-bits of the Owner UID, GID
	OwnerUidHigh uint16
	OwnerGidHigh uint16

	// Lower 16-bits of the inode checksum
	ChecksumLow uint16

	Reserved uint16

	// Size of this inode - 128. Alternately,
	// the size of the extended inode fields
	// beyond the original ext2 inode, including this field
	ExtraSize uint16

	// Upper 16-bits of the inode checksum
	ChecksumHigh uint16

	// Extra time bits. This provides sub-second precision
	ChangeTimeExtraPrecision   Time32
	ModTimeExtraPrecision      Time32
	AccessTimeExtraPrecision   Time32
	CreationTime               Time32
	CreationTimeExtraPrecision Time32

	// Upper 32-bits for version number
	VersionHigh uint32
}

// ReadFrom reads entry from r.
func (entry *InodeEntry) ReadFrom(r io.Reader) error {
	return binary.Read(r, binary.LittleEndian, entry)
}

// WriteTo writes entry to w.
func (entry *InodeEntry) WriteTo(w io.Writer) error {
	return binary.Write(w, binary.LittleEndian, entry)
}

// Bytes returns entry in binary form.
func (entry *InodeEntry) Bytes() []byte {

	buf := new(bytes.Buffer)
	entry.WriteTo(buf)

	return buf.Bytes()
}

const (
	ExtentTreeSignature = uint16(0xF30A)
	ExtentTreeEntrySize = 12
	ExtentTreeType      = Type + " extent tree"
)

// IndirectExtentTreeEntry is a raw entry, which points to a child extent tree node.
type IndirectExtentTreeEntry struct {
	FirstDataBlockIdx     uint32 // This extent tree covers file blocks from FirstDataBlockIdx onward.
	ChildNodeLocationLow  uint32 // Lower 32-bits of block number of the extent node that is the next level lower in the tree.
	ChildNodeLocationHigh uint16 // Upper 16-bits of previous field
	Reserved              uint16
}

// DirectExtentTreeEntry is a raw entry, which points to a file system extent.
type DirectExtentTreeEntry struct {
	FirstDataBlockIdx  uint32 // This extent tree covers file blocks from FirstDataBlockIdx onward.
	BlocksPerExtent    uint16 // Number of blocks covered by extent
	ExtentLocationHigh uint16 // Upper 16-bits of block number to which this extent points
	ExtentLocationLow  uint32 // Lower 32-bits of previous field
}

// ExtentTreeNode is a raw extent tree node on disk structure.
type ExtentTreeNode struct {
	Header struct {
		Signature    uint16 // Magic number, 0xF30A
		ValidEntries uint16 // Number of valid entries following the header
		TotalEntries uint16 // Maximum number of entries that could follow the header
		Depth        uint16 // Depth of this extent node in the extent tree
		Generation   uint32 // Generation of the tree
	}
	Entries  []unsafe.Pointer // if depth is equal to 0, then direct, otherwise indirect
	Reserved []byte           // bytes between last valid entry and checksum
	Checksum uint32
}

// ReadFrom reads node from r.
func (node *ExtentTreeNode) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, &node.Header)
	if err != nil {
		return err
	}

	err = VerifyFormatOf(ExtentTreeType, "signature", node.Header.Signature, ExtentTreeSignature)
	if err != nil {
		return err
	}

	valid, total := node.Header.ValidEntries, node.Header.TotalEntries
	node.Entries = make([]unsafe.Pointer, valid)

	for n := range node.Entries {

		if node.Header.Depth == 0 {
			node.Entries[n] = unsafe.Pointer(new(DirectExtentTreeEntry))
			err = binary.Read(r, binary.LittleEndian, (*DirectExtentTreeEntry)(node.Entries[n]))
		} else {
			node.Entries[n] = unsafe.Pointer(new(IndirectExtentTreeEntry))
			err = binary.Read(r, binary.LittleEndian, (*IndirectExtentTreeEntry)(node.Entries[n]))
		}

		if err != nil {
			return err
		}
	}

	reslen := total - valid
	if reslen > 0 {
		node.Reserved = make([]byte, reslen*ExtentTreeEntrySize)
		err = binary.Read(r, binary.LittleEndian, node.Reserved)
	}

	// if it's an inode, checksum could just not fit in iblock
	binary.Read(r, binary.LittleEndian, &node.Checksum)
	return err
}

// WriteTo writes node to w.
func (node *ExtentTreeNode) WriteTo(w io.Writer) error {

	err := binary.Write(w, binary.LittleEndian, &node.Header)
	if err != nil {
		return err
	}

	for n := range node.Entries {

		if node.Header.Depth == 0 {
			err = binary.Write(w, binary.LittleEndian, (*DirectExtentTreeEntry)(node.Entries[n]))
		} else {
			err = binary.Write(w, binary.LittleEndian, (*IndirectExtentTreeEntry)(node.Entries[n]))
		}

		if err != nil {
			return err
		}
	}

	_, err = w.Write(node.Reserved)

	binary.Write(w, binary.LittleEndian, &node.Checksum)
	return err
}

// Bytes returns node in binary form.
func (node *ExtentTreeNode) Bytes() []byte {

	buf := new(bytes.Buffer)
	node.WriteTo(buf)

	return buf.Bytes()
}

// FileType reprsents a file type specified in DirEntry.
type FileType uint8

const (
	UnknownFile = FileType(iota)
	RegularFile
	Directory
	CharacterDevice
	BlockDevice
	FIFO
	Socket
	SymbolicLink
)

// String returns plain text name of t.
func (t FileType) String() string {
	switch t {
	case UnknownFile:
		return "Unknown file"
	case RegularFile:
		return "Regular file"
	case Directory:
		return "Directory"
	case CharacterDevice:
		return "Character device"
	case BlockDevice:
		return "Block device"
	case FIFO:
		return "FIFO"
	case Socket:
		return "Socket"
	case SymbolicLink:
		return "Symbolic link"
	default:
		return "Invalid file"
	}
}

const (
	DirEntrySize = 263
	DirEntryType = Type + " directory entry"
	DirBlockType = Type + " directory block"
)

const FileNameMaxLen = 255

// DirEntryHeader is a raw fixed size header of a directory entry.
type DirEntryHeader struct {
	InodeNum   uint32   // Number of the inode that this directory entry points to
	Length     uint16   // Length of this directory entry
	NameLength uint8    // Length of the file name
	FileType   FileType // File type code
}

// DirEntry is a raw directory entry on disk structure.
type DirEntry struct {
	DirEntryHeader
	Name     []byte // File name
	Reserved []byte
}

// ReadFrom reads entry from r.
func (entry *DirEntry) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, &entry.DirEntryHeader)
	if err != nil {
		return err
	}

	namelen := entry.NameLength
	if namelen > FileNameMaxLen {
		namelen = FileNameMaxLen
	}

	entry.Name = make([]byte, namelen)
	_, err = io.ReadFull(r, entry.Name)
	if err != nil {
		return err
	}

	// 8 is size of first 4 fields of dir entry
	reslen := entry.Length - uint16(namelen) - 8
	if reslen > 0 {
		entry.Reserved = make([]byte, reslen)
		_, err = r.Read(entry.Reserved)
	}

	return err
}

// WriteTo writes entry to w.
func (entry *DirEntry) WriteTo(w io.Writer) error {

	err := binary.Write(w, binary.LittleEndian, &entry.DirEntryHeader)
	if err != nil {
		return err
	}

	_, err = w.Write(entry.Name)
	if err != nil {
		return err
	}

	_, err = w.Write(entry.Reserved)

	return err
}

// Bytes returns entry in binary form.
func (entry *DirEntry) Bytes() []byte {

	buf := new(bytes.Buffer)
	entry.WriteTo(buf)

	return buf.Bytes()
}

const (
	XattrSignature  = uint32(0xEA020000)
	XattrHeaderType = Type + " extended attribute header"

	XattrShortHeaderSize = 4
	XattrLongHeaderSize  = 32
)

var XattrNamePrefix = [...]string{
	"",                         // 0
	"user.",                    // 1
	"system.posix_acl_acess",   // 2
	"system.posix_acl_default", // 3
	"trusted.",                 // 4
	"",                         // 5
	"security.",                // 6
	"system.",                  // 7
	"system.richacl",           // 8
}

// XattrShortHeader is a raw short-format header of an extended attribute.
type XattrShortHeader struct {
	Signature uint32
}

// XattrShortHeader is a raw long-format header of an extended attribute.
type XattrLongHeader struct {
	Signature uint32
	Refs      uint32
	Blocks    uint32
	Hash      uint32
	Checksum  uint32
	Reserved  [2]uint32
}

// ReadFrom reads xahead from r.
func (xahead *XattrShortHeader) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, xahead)
	if err != nil {
		return err
	}

	return VerifyFormatOf(XattrHeaderType, "signature", xahead.Signature, XattrSignature)
}

// WriteTo writes xahead to w.
func (xahead *XattrShortHeader) WriteTo(w io.Writer) error {
	return binary.Write(w, binary.LittleEndian, xahead)
}

// ReadFrom reads xahead from r.
func (xahead *XattrLongHeader) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, xahead)
	if err != nil {
		return err
	}

	return VerifyFormatOf(XattrHeaderType, "signature", xahead.Signature, XattrSignature)
}

// WriteTo writes xahead to w.
func (xahead *XattrLongHeader) WriteTo(w io.Writer) error {
	return binary.Write(w, binary.LittleEndian, xahead)
}

// XattrEntryHeader is a raw fixed size header of an extended attribute.
type XattrEntryHeader struct {
	NameLen       uint8
	NameIndex     uint8
	ValueOff      uint16
	ValueInodeNum uint32
	ValueSize     uint32
	Hash          uint32
}

// XattrEntry is a raw extended attribute on disk structure.
type XattrEntry struct {
	XattrEntryHeader
	Name  string
	Value []byte
}

// ReadFrom reads entry from r.
func (entry *XattrEntry) ReadFrom(r io.Reader) error {

	err := binary.Read(r, binary.LittleEndian, &entry.XattrEntryHeader)
	if err != nil {
		return err
	}

	name := make([]byte, entry.NameLen)
	_, err = io.ReadFull(r, name)
	if err != nil {
		return err
	}

	entry.Name = string(name)
	if i := int(entry.NameIndex); i < len(XattrNamePrefix) {
		entry.Name = XattrNamePrefix[i] + entry.Name
	}

	return nil
}

// WriteTo writes entry to w.
func (entry *XattrEntry) WriteTo(w io.Writer) error {

	err := binary.Write(w, binary.LittleEndian, &entry.XattrEntryHeader)
	if err != nil {
		return err
	}

	if i := int(entry.NameIndex); i < len(XattrNamePrefix) {
		strings.TrimPrefix(entry.Name, XattrNamePrefix[i])
	}

	_, err = io.WriteString(w, entry.Name)
	return err
}
