package btree

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sort"
)

// Btree represents a btree ordered container
// for (int64, int64) key-value pair.
type Btree struct {
	degree  int
	height  int
	length  int
	deleted int
	root    *node
}

// Constants for degrees of a btree,
// which defines the max number of elements in a node.
const (
	DefaultDegree = 512
	MinDegree     = 2
)

// New returns a new instance of btree.
func New() *Btree {
	return NewWithDegree(DefaultDegree)
}

// NewWithDegree returns a new instance of btree with degree d.
func NewWithDegree(d int) *Btree {
	if d < MinDegree {
		d = MinDegree
	}
	return &Btree{degree: d, root: new(node), height: 1}
}

// Len returns number of (key, value) pairs in the btree.
func (t *Btree) Len() int {
	return t.length - t.deleted
}

// Cap returns actual number of (key, value) pairs in the btree, deleted and not clean up also.
func (t *Btree) Cap() int {
	return t.length
}

// Add replaces or inserts the (key, value) pair to the btree.
// Returns the replaced value, otherwise reports that the value wasn't replaced.
func (t *Btree) Add(key, value int64) (replacedValue int64, replaced bool) {

	maxChildren := 2*t.degree + 1

	if len(t.root.children) > maxChildren {
		t.height++
		t.root.stealMaxKey()
		t.root = &node{children: children{t.root, t.root.split(maxChildren / 2)}}
	}

	replacedValue, replaced = t.root.add(key, value, maxChildren, t.height)
	if !replaced {
		t.length++
	}
	return
}

// Find find and returns the value from the btree associated with the key.
func (t *Btree) Find(key int64) (foundValue int64, found bool) {
	foundNode := t.root.find(key)
	if foundNode != nil && !foundNode.deleted {
		foundValue, found = foundNode.value, true
	}
	return
}

// Delete deletes the value from the btree associated with the key.
func (t *Btree) Delete(key int64) (deletedValue int64, deleted bool) {
	foundNode := t.root.find(key)
	if foundNode != nil {
		deletedValue, deleted = foundNode.value, true
		foundNode.deleted = true
	}
	t.cleanUp()
	return
}

// ForEach calls f for every (key, value) pair from the btree.
// The calls are done in ascending keys order and they will be stopped, if f returns false.
func (t *Btree) ForEach(f func(key int64, value int64) bool) {
	for _, c := range t.root.children {
		if !c.forEach(f) {
			break
		}
	}
}

const (
	cleanUpPercent = 40.0
	cleanUpLength  = 100
)

// cleanUp reassembles the tree, if it's neccessary.
// TODO: should remove the pairs, don't reassmeble the btree.
func (t *Btree) cleanUp() {

	p := 100.0 * float64(t.deleted) / float64(t.length)

	if p < cleanUpPercent && t.length > cleanUpLength {
		return
	}

	newTree := NewWithDegree(t.degree)

	t.ForEach(func(key, value int64) bool {
		newTree.Add(key, value)
		return true
	})

	t.root = newTree.root
	t.height = newTree.height
	t.length = newTree.length

	t.deleted = 0
}

// Dump dumps the btree to the standard output.
func (t *Btree) Dump() error {
	return t.Fdump(os.Stdout)
}

// Sdump dumps the btree to the string.
func (t *Btree) Sdump() string {
	var b bytes.Buffer
	t.Fdump(&b)
	return b.String()
}

// Fdump dumps the btree to w.
func (t *Btree) Fdump(w io.Writer) error {
	return t.root.fdump(w, "")
}

// node is a node of a btree.
type node struct {
	key, value int64
	deleted    bool
	children   children
}

// String implements the fmt.Stringer String() method.
func (n *node) String() string {
	return fmt.Sprintf("<%d, %d>", n.key, n.value)
}

// stealMaxKey steals max key of the children of the node,
// without destroying the structure of the btree.
func (n *node) stealMaxKey() {

	for {
		c := n.children[len(n.children)-1]

		n.key, n.value = c.key, c.value
		n.deleted = c.deleted

		if len(c.children) > 0 {
			n = c
		} else {
			break
		}
	}

	n.children = n.children[:len(n.children)-1]
}

// split splits the i children of the node, returns the child, that should be next.
func (n *node) split(i int) *node {

	next := &node{n.key, n.value, n.deleted, nil}
	next.children = append(next.children, n.children[i+1:]...)

	n.children = n.children[:i+1]
	n.stealMaxKey()

	return next
}

// add adds the (key, value) pair to the node.
func (n *node) add(key, value int64, maxChildren, depth int) (replacedValue int64, found bool) {

	for ; depth > 0; depth-- {

		i, found := n.children.find(key)

		if found {
			n.children[i].value, replacedValue = value, n.children[i].value
			n.children[i].deleted = false
			return replacedValue, found
		}

		if depth <= 1 {
			n.children.addAt(i, &node{key: key, value: value})
			return replacedValue, found
		}

		if len(n.children) == 0 {
			panic("depth of the node > 1, but there is no children!")
		}

		var c *node

		if i == len(n.children) {

			i = i - 1
			c = n.children[i]

			n.children[i].key, key = key, n.children[i].key
			n.children[i].value, value = value, n.children[i].value

			if n.children[i].deleted {
				n.children[i].deleted = false
				return replacedValue, found
			}

		} else {
			c = n.children[i]
		}

		if len(c.children) > maxChildren {

			n.children.addAt(i+1, c.split(maxChildren/2))

			if c.key == key {
				c.value, replacedValue, found = value, c.value, true
				return replacedValue, found
			} else if c.key < key {
				c = n.children[i+1]
			}
		}

		n = c
	}

	return replacedValue, found
}

// find finds the node by the key.
func (n *node) find(key int64) (foundNode *node) {

	var i int
	var found bool

	for {
		i, found = n.children.find(key)
		if found {
			foundNode = n.children[i]
			break
		}
		if len(n.children) == i || len(n.children) == 0 {
			break
		}
		n = n.children[i]
	}

	return foundNode
}

// forEach calls the f function for every (key, value) pair of the node.
func (n *node) forEach(f func(key, value int64) bool) bool {

	for _, c := range n.children {
		if !c.forEach(f) {
			return false
		}
	}

	return !n.deleted && f(n.key, n.value)
}

// fdump dumps the node to w.
func (n *node) fdump(w io.Writer, prefix string) error {

	const (
		emptyItem    = "    "
		middleItem   = "├── "
		continueItem = "│   "
		lastItem     = "└── "
	)

	if prefix == "" {
		_, err := fmt.Fprintln(w, ".")
		if err != nil {
			return err
		}
	}

	for i, c := range n.children {

		var item string
		var childItem string

		if i == len(n.children)-1 {
			item, childItem = lastItem, emptyItem
		} else {
			item, childItem = middleItem, continueItem
		}

		_, err := fmt.Fprintln(w, prefix+item, c)
		if err != nil {
			return err
		}

		err = c.fdump(w, prefix+childItem)
		if err != nil {
			return err
		}
	}

	return nil
}

// children is a simple container for children of a node
// (which is, actually, sorted array).
type children []*node

// find finds the index of the child by the key.
func (c children) find(key int64) (i int, found bool) {
	i = sort.Search(len(c), func(i int) bool {
		return key < c[i].key
	})
	if i > 0 && c[i-1].key == key {
		i, found = i-1, true
	}
	return i, found
}

// addAt adds the n on the i position.
func (c *children) addAt(i int, n *node) {
	*c = append(*c, nil)
	if i+1 < len(*c) {
		copy((*c)[i+1:], (*c)[i:])
	}
	(*c)[i] = n
}
