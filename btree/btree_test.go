package btree_test

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/nyakushevskiy/filesystems/btree"
)

const MagicValue = ('M' << 0) | ('A' << 8) | ('G' << 16) | ('I' << 24) | ('C' << 32)

func ASCII(v int64) string {
	var b []byte
	for shift := uint(0); shift < 64 && byte(v) != 0; shift += 8 {
		b = append(b, byte(v))
		v >>= 8
	}
	return string(b)
}

func AssertFound(t *btree.Btree, minKey, maxKey int64, expected func(k int64) (int64, bool)) error {
	for k := minKey; k <= maxKey; k++ {
		v, f := t.Find(k)
		if value, found := expected(k); f != found {
			if f {
				return fmt.Errorf("%d key expected to be not found, but it is!", k)
			} else {
				return fmt.Errorf("%d key expected to be found, but it's not!", k)
			}
		} else if f && v != value {
			return fmt.Errorf("Value for %d key! Got: %d (%s), expected: %d (%s).", k, v, ASCII(v), value, ASCII(value))
		}
	}
	return nil
}

const Add = 1000

func TestAddAscending(t *testing.T) {

	btree := btree.New()

	for n := 0; n < Add; n++ {
		k := 2 * int64(n)
		btree.Add(k, MagicValue)
	}

	err := AssertFound(btree, 0, 2*(Add-1), func(k int64) (int64, bool) {
		return MagicValue, (k % 2) == 0
	})
	if err != nil {
		t.Error(err)
	}
}

func TestAddRandom(t *testing.T) {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	random := map[int64]bool{}

	btree := btree.New()

	for n := 0; n < Add; n++ {
		k := r.Int63n(2 * Add)
		random[k] = true
		btree.Add(k, MagicValue)
	}

	err := AssertFound(btree, 0, 2*Add-1, func(k int64) (int64, bool) {
		return MagicValue, random[k]
	})
	if err != nil {
		t.Error(err)
	}
}

func TestDelete(t *testing.T) {

	btree := btree.New()

	for n := 0; n < Add; n++ {
		k := int64(n)
		btree.Add(k, MagicValue)
	}

	for n := 0; n < (Add / 4); n++ {
		k := 2 * int64(n)
		btree.Delete(k)
	}

	err := AssertFound(btree, 0, Add-1, func(k int64) (int64, bool) {
		return MagicValue, ((k % 2) == 1) || (k >= Add/2)
	})
	if err != nil {
		t.Error(err)
	}
}

func TestDeleteToCleanUp(t *testing.T) {

	btree := btree.New()

	for n := 0; n < Add; n++ {
		k := int64(n)
		btree.Add(k, MagicValue)
	}

	// deleting all even keys, which is 50%
	for n := 0; n < (Add / 2); n++ {
		k := 2 * int64(n)
		btree.Delete(k)
	}

	err := AssertFound(btree, 0, Add-1, func(k int64) (int64, bool) {
		return MagicValue, (k % 2) == 1
	})
	if err != nil {
		t.Error(err)
	}
}

type TestBtree interface {
	Add(int64, int64) (int64, bool)
	Find(int64) (int64, bool)
}

// TestMap is a sort of good container for this purposes.
type TestMap map[int64]int64

func (m TestMap) Add(key, value int64) (int64, bool) {
	replacedValue, replaced := m[key]
	m[key] = value
	return replacedValue, replaced
}

func (m TestMap) Find(key int64) (int64, bool) {
	foundValue, found := m[key]
	return foundValue, found
}

type BenchmarkMode int

const (
	FindBenchmark = BenchmarkMode(iota)
	AddBenchmark
)

func benchmark(container TestBtree, b *testing.B, mode BenchmarkMode) {

	b.StopTimer()

	var keys []int64

	for len(keys) < b.N {
		keys = append(keys, rand.Int63())
	}

	if mode == AddBenchmark {
		b.StartTimer()
	}

	for i := 0; i < b.N; i++ {
		container.Add(keys[i], MagicValue)
	}

	if mode == FindBenchmark {
		b.StartTimer()
	} else {
		b.StopTimer()
	}

	ok := true

	for i := 0; i < b.N; i++ {
		value, found := container.Find(keys[i])
		ok = ok && value == MagicValue && found
	}

	if !ok {
		b.Fatal("Got wrong value!")
	}
}

func BenchmarkAddToBtreeDegree2(b *testing.B) {
	benchmark(btree.NewWithDegree(2), b, AddBenchmark)
}

func BenchmarkAddToBtreeDegree8(b *testing.B) {
	benchmark(btree.NewWithDegree(8), b, AddBenchmark)
}

func BenchmarkAddToBtreeDegree64(b *testing.B) {
	benchmark(btree.NewWithDegree(64), b, AddBenchmark)
}

func BenchmarkAddToBtreeDegree256(b *testing.B) {
	benchmark(btree.NewWithDegree(256), b, AddBenchmark)
}

func BenchmarkAddToBtreeDegree1024(b *testing.B) {
	benchmark(btree.NewWithDegree(1024), b, AddBenchmark)
}

func BenchmarkAddToMap(b *testing.B) {
	benchmark(TestMap{}, b, AddBenchmark)
}

func BenchmarkFindInBtreeDegree2(b *testing.B) {
	benchmark(btree.NewWithDegree(2), b, FindBenchmark)
}

func BenchmarkFindInBtreeDegree8(b *testing.B) {
	benchmark(btree.NewWithDegree(8), b, FindBenchmark)
}

func BenchmarkFindInBtreeDegree64(b *testing.B) {
	benchmark(btree.NewWithDegree(64), b, FindBenchmark)
}

func BenchmarkFindInBtreeDegree256(b *testing.B) {
	benchmark(btree.NewWithDegree(256), b, FindBenchmark)
}

func BenchmarkFindInBtreeDegree1024(b *testing.B) {
	benchmark(btree.NewWithDegree(1024), b, FindBenchmark)
}

func BenchmarkFindInMap(b *testing.B) {
	benchmark(TestMap{}, b, FindBenchmark)
}
