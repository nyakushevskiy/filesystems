# README #

This repo contains tasks for DATI course on modern file systems.

### TASKS ###

Here you can see 3 directories with tasks:

1. **ut8**\
   very simple golang-package for encoding-decoding unicode symbols to utf-8 with auto tests.
2. **procfs**\
   very simple golang-package "proc" and cli-tool based on it, which simulates "proc" and "lsof" linux commands.
3. **ext4fs**\
   not very simple golang-package "ext4" and cli-tool for testing its functionality.